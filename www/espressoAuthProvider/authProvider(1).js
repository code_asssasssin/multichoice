// A very simple authentication provider for Espresso Logic
//
// A JavaScript authentication provider must be a function returning an object
// with four functions, as shown here.
// See http://docs.espressologic.com/docs/logic-designer/security/authentication/custom-authentication-provider
// for a lot more details.

function SQLAuthenticationProviderCreate() {
    // Default values for the configuration parameters
    var dbUrl = '';
    var database = '';
    var user = '';
    var password = '';
    var keyLifetimeMinutes = 0;

    // This gets called with the user-specified values for the parameters described by getConfigInfo
    function configure(myConfig) {
        dbUrl = myConfig.dbUrl || '';
        database = myConfig.database || '';
        user = myConfig.user || '';
        password = myConfig.password || '';
        keyLifetimeMinutes = myConfig.keyLifetimeMinutes || 60;
    }

    // This is the method called to do the authentication
    // The payload is an object with properties as defined by the getLoginInfo() call
    function authenticate(payload) {
        if (payload.unregistered) {
            return {
                errorMessage: null,
                roleNames: ['unregistered']
            };
        }
        //var result;
        /*var scriptRunner = new com.kahuna.logic.lib.sql.ScriptRunner(dbUrl + ';' + database, database, user, password);
        var result = scriptRunner.query("EXEC LoginUser @username = 'dyoungikem', @password = 'Elivong619'");
        java.lang.System.out.println(result);*/
        java.lang.Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        var url = "jdbc:sqlserver://" + dbUrl + ";databaseName=" + database + ";user=" + user + ";password=" + password;
        var con = java.sql.DriverManager.getConnection(url);
        var stmt = con.createStatement();
        var sql = "EXEC LoginUser @username = '" + payload.username + "', @password = '" + payload.password + "'";
        //var sql = "SELECT Id, Email, FirstName, LastName FROM Users WHERE UserName = 'dyoungikem' AND PasswordHash = HASHBYTES('SHA2_256','Elivong619')"
        var rst = stmt.executeQuery(sql);
        var result = {};
        while (rst.next()) {
            result.Id = rst.getString("Id") + "";
            result.Roles = rst.getString("Roles") + "";
            result.Name = rst.getNString("LastName") + " " + rst.getNString("FirstName");
            result.Email = rst.getNString("Email") + "";
        }
        rst.close();
        stmt.close();
        con.close();

        if (result.Id != null) {
            try {
                return {
                    errorMessage: null,
                    roleNames: result.Roles.split(","),
                    userData: result,
                    keyLifetimeSeconds: keyLifetimeMinutes * 60,
                 //   keyExpiration: new Date(+new Date() + (+keyLifetimeMinutes) * 60 * 1000),
                    userInfo: result,
                    userIdentifier: result.Id,
                }
            } catch (e) {
                return {
                    errorMessage: 'An Error Occurred'
                };
            }
        } else
            return {
                errorMessage: 'No user found'
            };
    }

    // This gets called when a client needs to know what information is required to authenticate.
    // The information returned by this will be provided to the client, who can then make a call
    // with values for the parameters declared here.
    function getLoginInfo() {
        //@login_info
        return {
            fields: [{
                name: 'username',
                display: 'username',
                description: 'UserName',
                type: 'text',
                length: 1
            }, {
                name: 'password',
                display: 'password',
                description: 'User Password',
                type: 'password',
                length: 1
            }],
        };
    }

    // This gets called when the authentication provider is initialized.
    // It should return an object describing what parameters it needs to get configured.
    // The user will fill in these parameters in the Logic Designer, and when they save,
    // the configure function (above) will get called with values for these parameters.
    function getConfigInfo() {
        return {
            // The current values for the configuration parameters described below.
            // This allows the Logic Designer to show these values as already set by default.
            current: {
                user: user,
                password: password,
                dbUrl: dbUrl,
                database: database,
                keyLifetimeMinutes: keyLifetimeMinutes
            },

            // The specification for our configuration parameters.
            // In this example, we want a phrase to greet the user, and
            // a secret word you must know to get in.
            fields: [{
                name: 'dbUrl',
                display: 'Database URL',
                description: 'Database URL',
                length: 64,
                helpURL: ''
            }, {
                name: 'database',
                display: 'Database Name',
                description: 'Database Name',
                length: 64,
                helpURL: ''
            }, {
                name: 'user',
                display: 'Database User',
                description: 'Database User',
                length: 64,
                helpURL: ''
            }, {
                name: 'password',
                display: 'User Password',
                description: 'User Password',
                length: 64,
                helpURL: ''
            }, {
                name: "keyLifetimeMinutes",
                display: "API Key Lifetime (Minutes)",
                type: "number",
                length: 8,
                helpURL: "http://www.espressologic.com"
            }]
        };
    }

    // This is the authentication object that Espresso Logic will call on your behalf.
    // getConfigInfo() is used by the Logic Designer
    // getLoginInfo() is invoked when the client calls
    // authenticate() is used when you post to @authentication Live Browser (and your own applications)
    return {
        configure: configure,
        authenticate: authenticate,
        getLoginInfo: getLoginInfo,
        getConfigInfo: getConfigInfo
    };
}

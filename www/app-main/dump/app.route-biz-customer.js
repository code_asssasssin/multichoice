(function () {
    'use strict';
    angular.module('customermobile.routes').config(Config);
    Config.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];
    /* @ngInject */
    function Config($stateProvider, $urlRouterProvider, $locationProvider) {
        var viewDir = "AppMain/views/";
 
        $stateProvider
            .state('app.biz-customer-level', {
                url: "^/biz/customer/level",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "biz-customer-level.html",
                        controller: 'customerBizCtrl',
                        controllerAs: "customer"
                    }
                } 
            })              
            .state('app.biz-customer-life-cycle', {
                url: "^/biz/customer/life-cycle",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "biz-customer-life-cycle.html",
                        controller: 'customerBizCtrl',
                        controllerAs: "customer"
                    }
                } 
            })              
  
        /////////////////////
        ; // WRAP
        /////////////////////
    }
})();
(function () {
    'use strict';
    angular.module('customermobile.routes').config(Config);
    Config.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];
    /* @ngInject */
    function Config($stateProvider, $urlRouterProvider, $locationProvider) {
        var viewDir = "AppMain/views/";

        $stateProvider
            .state('app.shop-compare-price', {
                url: "^/shop/compare-price",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "shop-compare-price.html",
                        controller: 'shopCtrl',
                        controllerAs: "shop"
                    }
                }
            })            
            .state('app.shop-any-product', {
                url: "^/shop/any-product",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "shop-any-product.html",
                        controller: 'shopCtrl',
                        controllerAs: "shop"
                    }
                }
            })            
            .state('app.shop-packaged-product', {
                url: "^/shop/packaged-product",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "shop-packaged-product.html",
                        controller: 'shopCtrl',
                        controllerAs: "shop"
                    }
                }
            })            
            .state('app.shop-view-cart', {
                url: "^/shop/view-cart",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "shop-view-cart.html",
                        controller: 'shopCtrl',
                        controllerAs: "shop"
                    }
                }
            })            
            .state('app.shop-recent-order', {
                url: "^/shop/recent-order",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "shop-recent-order.html",
                        controller: 'shopCtrl',
                        controllerAs: "shop"
                    }
                }
            })

        /////////////////////
        ; // WRAP
        /////////////////////
    }
})();
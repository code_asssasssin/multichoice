(function () {
    'use strict';
    angular.module('customermobile.routes').config(Config);
    Config.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];
    /* @ngInject */
    function Config($stateProvider, $urlRouterProvider, $locationProvider) {
        var viewDir = "AppMain/views/";
 
        $stateProvider
            .state('app.news-view', {
                url: "^/news/view",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "news-view.html",
                        controller: 'newsCtrl',
                        controllerAs: "news"
                    }
                } 
            })               
            .state('app.news-archived', {
                url: "^/news/archived",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "news-archived.html",
                        controller: 'newsCtrl',
                        controllerAs: "news"
                    }
                } 
            })            

        /////////////////////
        ; // WRAP
        /////////////////////
    }
})();
(function () {
    'use strict';
    angular.module('customermobile.routes').config(Config);
    Config.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];
    /* @ngInject */
    function Config($stateProvider, $urlRouterProvider, $locationProvider) {
        var viewDir = "AppMain/views/";

        $stateProvider         
            .state('app.customer-profile-view', {
                url: "^/customer/profile/view",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "customer-profile-view.html",
                        controller: 'customerCtrl',
                        controllerAs: "customer"
                    }
                }
            })              
            .state('app.customer-profile-edit', {
                url: "^/customer/profile/edit",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "customer-profile-edit.html",
                        controller: 'customerCtrl',
                        controllerAs: "customer"
                    }
                }
            })              
            .state('app.customer-feedback', {
                url: "^/customer/feedback",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "customer-feedback.html",
                        controller: 'customerCtrl',
                        controllerAs: "customer"
                    }
                }
            })               

        /////////////////////
        ; // WRAP
        /////////////////////
    }
})();
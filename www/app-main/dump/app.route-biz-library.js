(function () {
    'use strict';
    angular.module('customermobile.routes').config(Config);
    Config.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];
    /* @ngInject */
    function Config($stateProvider, $urlRouterProvider, $locationProvider) {
        var viewDir = "AppMain/views/";
 
        $stateProvider
            .state('app.biz-library-customer-statement', {
                url: "^/biz/library/customer-statement",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "biz-library-customer-statement.html",
                        controller: 'libraryBizCtrl',
                        controllerAs: "library"
                    }
                } 
            })                  
            .state('app.biz-library-agreements', {
                url: "^/biz/library/agreements",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "biz-library-agreements.html",
                        controller: 'libraryBizCtrl',
                        controllerAs: "library"
                    }
                } 
            })             
            .state('app.biz-library-invoice', {
                url: "^/biz/library/invoice",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "biz-library-invoice.html",
                        controller: 'libraryBizCtrl',
                        controllerAs: "library"
                    }
                } 
            })               
  
        /////////////////////
        ; // WRAP
        /////////////////////
    }
})();
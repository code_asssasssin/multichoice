(function () {
    'use strict';
    angular.module('customermobile.routes').config(Config);
    Config.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];
    /* @ngInject */
    function Config($stateProvider, $urlRouterProvider, $locationProvider) {
        var viewDir = "AppMain/views/";
 
        $stateProvider
            .state('app.biz-sales-ageing-report', {
                url: "^/biz/sales/ageing-report",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "biz-sales-ageing-report.html",
                        controller: 'salesBizCtrl',
                        controllerAs: "sales"
                    }
                } 
            })              
            .state('app.biz-sales-statement', {
                url: "^/biz/sales/statement",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "biz-sales-statement.html",
                        controller: 'salesBizCtrl',
                        controllerAs: "sales"
                    }
                } 
            })              
            .state('app.biz-sales-transaction', {
                url: "^/biz/sales/transaction",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "biz-sales-transaction.html",
                        controller: 'salesBizCtrl',
                        controllerAs: "sales"
                    }
                } 
            })              
            .state('app.biz-sales-invoice', {
                url: "^/biz/sales/invoice",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "biz-sales-invoice.html",
                        controller: 'salesBizCtrl',
                        controllerAs: "sales"
                    }
                } 
            })             
            .state('app.biz-sales-past-requisition', {
                url: "^/biz/sales/past-requisition",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "biz-sales-past-requisition.html",
                        controller: 'salesBizCtrl',
                        controllerAs: "sales"
                    }
                } 
            })               
  
        /////////////////////
        ; // WRAP
        /////////////////////
    }
})();
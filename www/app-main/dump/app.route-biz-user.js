(function () {
    'use strict';
    angular.module('customermobile.routes').config(Config);
    Config.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];
    /* @ngInject */
    function Config($stateProvider, $urlRouterProvider, $locationProvider) {
        var viewDir = "AppMain/views/";
 
        $stateProvider
            .state('app.biz-user-feedback-report', {
                url: "^/biz/user/feedback-report",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "biz-feedback-report.html",
                        controller: 'userBizCtrl',
                        controllerAs: "user"
                    }
                } 
            })               
            .state('app.biz-user-feedback-export', {
                url: "^/biz/user/feedback-export",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "biz-user-feedback-export.html",
                        controller: 'userBizCtrl',
                        controllerAs: "user"
                    }
                } 
            })               
  
        /////////////////////
        ; // WRAP
        /////////////////////
    }
})();


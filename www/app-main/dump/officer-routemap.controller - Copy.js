(function () {
    'use strict';
    angular.module('sinet.controllers').controller('officerRouteCtrl', Ctrl);
    Ctrl.$inject = ['$q', '$state', '$scope', 'regData', '$ionicLoading', '$compile'];
    /* @ngInject */
    function Ctrl($q, $state, $scope, registrationData, $ionicLoading, $compile) {
        //var vm = this;
        $scope.initialize = function () {
            var myLatlng = new google.maps.LatLng(43.07493, -89.381388);

            var mapOptions = {
                center: myLatlng,
                zoom: 16,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("map"),
                mapOptions);

            var mapIcon = "img/policeman.png";
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                icon: mapIcon
            });

            google.maps.event.addListener(marker, 'click', function () {
                infowindow.open(map, marker);
            });

            $scope.map = map;
        };
        
        //$scope.map = { center: { latitude: 45, longitude: -73 }, zoom: 8 };
        
        console.log('$scope.map');
        console.log($scope.map);
    }
})();
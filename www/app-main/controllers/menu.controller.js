(function () {
    'use strict';
    Array.prototype.contains = function (val) {
        var i = this.length;
        while (i--) {
            /* var j = arr.length;
               while (j--) {
                 if (this[i] === arr[j]) {
                   return true;
                 }
               }*/
            if (this[i] === val) {
                return true;
            }
        }
        return false;
    };
    angular.module('sinet.controllers').controller('menuCtrl', menuCtrl);
    menuCtrl.$inject = ['$q', '$state', 'menuService', 'authService', 'localStorageService', 'modalDialogService', 'appSettingSevice', '$scope', '$ionicSideMenuDelegate'];
    /* @ngInject */
    function menuCtrl($q, $state, menuService, authService, localStorageService, modalDialogService, appSettingSevice, $scope, $ionicSideMenuDelegate) {
        //var menuData = menuService.menu();
        var vm = $scope;
        vm.menuData = {
            title: 'Main Menu',
            items: []
        };

        $scope.toggleLeft = function () {
            $ionicSideMenuDelegate.toggleLeft();
        };
        var viewDir = appSettingSevice.viewDir;

        $scope.$on('logged-in', function (ev, val) {
            console.log(ev, 'val=' + val);
            vm.menuData.items = [];
            buildMenu();
        });

        function buildMenu() {
            var menuItems = $state.get();
            menuItems.forEach(function (i) {
                if (i.views) {
                    var item = i.views.menuContent;
                    if (item.hasOwnProperty('title')) {
                        item.data = item.data || {};
                        if (item.data.role.contains(authService.authentication.roles))
                            vm.menuData.items.push({
                                title: item.title,
                                name: i.name,
                            });
                    }
                }
            });
        }

        buildMenu();
        vm.gotoUrl = function (urlState) {
            $state.go(urlState);
        }

        function loginModal1() {
            modalDialogService.show(viewDir + "auth-signup-modal.html");
        }

        function loginModal2() {
            modalDialogService.show(viewDir + "auth-login-modal.html");
        }

        function login() {
            $state.go('app.auth-login');
        }

        function modalhide() {
            modalDialogService.hide();
        }
    }
})();
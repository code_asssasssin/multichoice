(function () {
    'use strict';

    angular.module('sinet.controllers').controller('logoffCtrl', logoff);
    logoff.$inject = ['authService', '$state', 'notificationService'];

    function logoff(authservice, state, notitficationService) {
       
            state.go('app.auth-login');

    }
})();
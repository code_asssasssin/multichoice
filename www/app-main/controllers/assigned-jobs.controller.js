(function() {
  'use strict';
  angular.module('sinet.controllers').controller('assignedJobsCtrl', assignedJobsCtrl);
  assignedJobsCtrl.$inject = ['$q', '$state', 'authService', 'assignedJobsDataService', '$ionicActionSheet', '$scope', 'notificationService'];
  /* @ngInject */
  function assignedJobsCtrl($q, $state, authService, assignedJobs, $ionicActionSheet, $scope, notificationService) {
    $scope.assignedJobsData = [];
    assignedJobs.getAllJobs(authService.authentication.id).then(function(result) {
      $scope.assignedJobsData = result;
    });


    $scope.showBottomSheet = function(job) {
      var updateText = 'Update Record To Completed';
      if (job.StatusId == 5)
        updateText = 'Update Record To Incompleted'

      var hideSheet = $ionicActionSheet.show({
        buttons: [{
          text: '<b>Show Directions</b>'
        }, {
          text: updateText
        }, {
          text: 'Show Details'
        }],
        titleText: 'Execute Actions',
        cancelText: 'Cancel',
        cancel: function() {
          hideSheet();
        },
        buttonClicked: function(index) {
          if (index == 0)
            $state.go('app.direction', {
              id: job.Customer.Id
            });
          else if (index == 1) {
            var StatusId = 5;
            if (job.StatusId == 5)
              StatusId = 4;
            assignedJobs.updateJobStatus({
              StatusId: StatusId,
              Id: job.Id
            }).then(function(result) {
              notificationService.success('Status Updated Successfully');
              job.StatusId = StatusId;
              hideSheet();
            });
          }
        }
      });
    }
  }
})();


(function() {

  'use strict';
  angular.module('sinet.controllers').controller('custOrderCtrl', Ctrl);
  Ctrl.$inject = ['$q', '$state', '$scope', 'authService', 'appSettingSevice', 'roleDataService', 'notificationService', 'localStorageService', 'productDataService', 'agentDataService', 'regSalesData', 'customerSalesDataService'];
  /* @ngInject */
  function Ctrl($q, $state, $scope, authService, appSettingService, roleDataService, notificationService, localStorageService, productDataService, agentDataService, regSalesData, customerSalesDataService) {
    var vm = $scope;

    vm.salesData = regSalesData.regData;
    vm.productList = [];
    var products = [];
    var productCount = -1;
    vm.agentList = [];

    var auth = localStorageService.get('authorizationData');




    productDataService.getAllProducts().then(function(result) {
      vm.productList = result;

    }, function(err) {

      notificationService.error('Network Error');

    });

    agentDataService.getAllAgents().then(function(response) {
      vm.agentList = response.result[0].rows;
      console.log(JSON.stringify(vm.agentList));
    }, function(err) {

      notificationService.error('Network Error');
    });


    vm.createOrder = function() {


      var salesId = "";
      var data = {};

      var latlng = {};

      getLongLagFromAddress(vm.salesData.address).then(
        function(result) {
          vm.salesData.sales.long = result.geometry.location.lng();
          vm.salesData.sales.latt = result.geometry.location.lat();

          vm.salesData.sales.salesAgentId = authService.authentication.id;

          customerSalesDataService.createAgentAndSales(vm.salesData).then(function(response) {
            salesId = response.procresults[0].result[0].rows[0].SalesOrderID;
            data = {
              SalesId: salesId,
              ProductId: vm.salesData.sales.salesProduct.productId,
            };

            if (data.SalesId != "" && data.ProductId != "") {
              notificationService.success(salesId);

              productDataService.addSalesProduct(data).then(function(response) {
                notificationService.success("Sales Created");
                vm.salesData = {};
                regSalesData.regData = {};
                $state.go('app.register-user');
              }, function(err) {

              });

            }

          }, function(err) {});


        },
        function() {

        }
      );
    };


    var addSales = function(salesObj) {

    };

    var addSalesProduct = function(salesProductObj) {

    };



    var getLongLagFromAddress = function(address) {
      var deferred = $q.defer();
      var latlng = {
        lat: "",
        lng: ""
      };

      var geocoder = new google.maps.Geocoder();
      geocoder.geocode({
          'address': address
        },
        function(results, status) {
          if (status === google.maps.GeocoderStatus.OK) {
            deferred.resolve(results[0]);

          } else {
            //deferred.reject(results);
          }


        });

      return deferred.promise;
    };

    vm.loadNearestAgentFieldMap = function() {
      var latlong = {};

      getLongLagFromAddress(vm.salesData.address).then(function(result) {

        latlong.lat = result.geometry.location.lat();
        vm.salesData.sales.long = result.geometry.location.lng();
        vm.salesData.sales.latt = result.geometry.location.lat();

        vm.salesData.sales.salesAgentId = authService.authentication.id;

        $state.go('app.installers-location');

      }, function(err) {
        notificationService.error("Please Retry");
      });


    };

    return vm;
  }


})();

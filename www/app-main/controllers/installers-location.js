(function () {
    'use strict';
    angular.module('sinet.controllers').controller('installerLocationCtrl', Ctrl);
    Ctrl.$inject = ['$q', '$state', '$scope', 'uiGmapGoogleMapApi', 'mapDataResolve', 'regSalesData'];
    /* @ngInject */
    function Ctrl($q, $state, $scope, uiGmapGoogleMapApi, mapDataResolve, regSalesData) {
        var vm = $scope;
        //vm.markers = [];

        vm.customerData = regSalesData.regData;
        vm.installerDetails = [
            {
                name: "Biyi Ekunwe",
                phoneNumber: "08235454547"
            },
            {
                name: "Olaolu Idowu",
                phoneNumber: "08199263536"
            },
            {
                name: "Miley Cyrus",
                phoneNumber: "08063534352"
            },
            {
                name: "Kim Kadarshian",
                phoneNumber: "081736363699"
            },
            {
                name: "Laide Omidare",
                phoneNumber: "080022764474"
            },
            {
                name: "Joe Igbokwe",
                phoneNumber: "08176253435"
            }];
        var latVal, longVal = "";

        try {
            if (vm.customerData != "") {
                latVal = vm.customerData.sales.latt;
                longVal = vm.customerData.sales.long;

            }
        } catch (err) {


            latVal = mapDataResolve.Lat;
            longVal = mapDataResolve.Long;

        }

        vm.markerIcon = "installerMain.png";

        uiGmapGoogleMapApi.then(function (maps) {
            vm.map = {
                center: {
                    latitude: latVal,
                    longitude: longVal
                },
                zoom: 16,
                bounds: {}
            };
        });

        vm.options = {
            scrollwheel: true
        };

        var createRandomMarker = function (i, bounds, idKey) {

            if (idKey === null) {
                idKey = "id";
            }

            var latitude = latVal + (Math.random() * 0.0065600000000000);
            var longitude = longVal + (Math.random() * 0.0065833300000049300);
            var ret = {};
            if (i == 5) {
                ret = {
                    latitude: latVal,
                    longitude: longVal,
                    title: 'm' + i,
                    id: i,
                    icon: "img/installerMain.png"

                };

                return ret;

            } else {
                ret = {
                    latitude: latitude,
                    longitude: longitude,
                    title: 'm' + i,
                    id: i,
                    icon: "img/installer.png"

                };

                return ret;
            }
        };
        
        function attachMessage(marker, installerDetail) {
			var windowTitle;
			var message;
            
            console.log(installerDetail);
			/*if (installerDetail.name !="") {
				windowTitle = 'Agent Info';
				message = 
                    '<div>' +
                        '<h2 >' + windowTitle+ '</h2>' +
                        '<p>' +
                            '<div class="row">'+
                                '<div class="col"><b>Agent Name:</b>'+ installerDetail.name+'</div>'+
                               '<div class="col"><b>Phone:</b>' + installerDetail.phoneNumber+'</div>'+
                            '</div>'+
                        '</p>' +
					'</div>';
			} 
            
           

			var infowindow = new google.maps.InfoWindow({
				content: message,
				size: new google.maps.Size(50, 50)
			});
			google.maps.event.addListener(marker, 'click', function () {
				infowindow.open(vm.data.myMap, marker);
			});*/
		}

        vm.randomMarkers = [];
        // Get the bounds from the map once it's loaded
        vm.$watch(function () {

            return vm.map.bounds;
        }, function (nv, ov) {

            // Only need to regenerate once
            if (!ov.southwest && !nv.southwest) {
                var markers = [];
                for (var i = 0; i < vm.installerDetails.length; i++) {
                    var marker = createRandomMarker(i, vm.map.bounds);
                                        //attachMessage(marker, vm.installerDetails[1]);

                    markers.push(marker);

                }
                vm.randomMarkers = markers;
            }
        }, true);

        //$scope.installerRoute = vm;
    }
})();
(function() {
  'use strict';
  angular.module('sinet.controllers').controller('CustDirectionCtrl', CustDirectionCtrl);
  CustDirectionCtrl.$inject = ['$q', '$scope', '$state', 'authService', 'notificationService', '$timeout', 'myCoordinate', 'customerCoordinate' ];


  function CustDirectionCtrl($q, $scope, state, authService, notificationService, $timeout, myCoordinate, customerCoordinate) {
    $scope.trip = {};
    $scope.routes = [];
    $scope.data = {};

    var directionsDisplay;
    var stepDisplay;
    var markerArray = [];

    console.log(myCoordinate);
    $scope.initializeMapControls = function() {
      $scope.data.mapOptions = {
        center: new google.maps.LatLng(myCoordinate.Lat, myCoordinate.Long),
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      $timeout(function() {
        directionsDisplay = new google.maps.DirectionsRenderer();

        //this is where we pass our the map object to the directionsDisplay.setMap method
        directionsDisplay.setMap($scope.data.myMap);
        directionsDisplay.setPanel(document.getElementById('directions-panel'));
        stepDisplay = new google.maps.InfoWindow();
        google.maps.event.trigger($scope.data.myMap, 'resize');
      }, 0);
    }
    $scope.initializeMapControls();

    $scope.setZoomMessage = function(zoom) {
      $scope.zoomMessage = 'You just zoomed to ' + zoom + '!';
      console.log(zoom, 'zoomed');
    };
    $scope.findPath = function() {

      //using the direction service of google map api
      $scope.directionsService = new google.maps.DirectionsService();

      var request = {
        origin: new google.maps.LatLng(myCoordinate.Lat, myCoordinate.Long),
        destination: new google.maps.LatLng(customerCoordinate.Latitude, customerCoordinate.Longitude),
        travelMode: google.maps.DirectionsTravelMode.DRIVING
      };
      //call the route method on google map api direction service with the request
      //which returns a response which can be directly provided to
      //directiondisplay object to display the route returned on the map
      $scope.directionsService.route(request, function(response, status) {

        if (status == google.maps.DirectionsStatus.OK) {
          console.log(response);
          directionsDisplay.setDirections(response);
          console.log(response.routes.length);
          showSteps(response);
          $scope.distance = response.routes[0].legs[0].distance.value / 1000;
        }
      });
    }

    function showSteps(directionResult) {
      // For each step, place a marker, and add the text to the marker's
      // info window. Also attach the marker to an array so we
      // can keep track of it and remove it when calculating new
      // routes.
      var myRoute = directionResult.routes[0].legs[0];

      for (var i = 0; i < myRoute.steps.length; i++) {
        var marker = new google.maps.Marker({
          position: myRoute.steps[i].start_location,
          map: $scope.data.myMap
        });
        attachInstructionText(marker, myRoute.steps[i].instructions);
        markerArray[i] = marker;
      }
    }

    function attachInstructionText(marker, text) {
      google.maps.event.addListener(marker, 'click', function() {
        // Open an info window when the marker is clicked on,
        // containing the text of the step.
        stepDisplay.setContent(text);
        stepDisplay.open($scope.data.myMap, marker);
      });
    }
    $scope.findPath();
  }

})();

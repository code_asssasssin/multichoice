(function () {

    'use strict';
    angular.module('sinet.controllers').controller('changeInstallerCtrl', Ctrl);
    Ctrl.$inject = ['$q', '$state', '$scope', 'authService', 'appSettingSevice', 'customerSalesDataService','notificationService','openSalesData','agentDataService','regSalesData'];
    /* @ngInject */
    function Ctrl($q, $state, $scope, authService, appSettingService, customerSalesDataService, notificationService,openSalesData,agentDataService,regSalesData) {
        var vm = $scope;
        
       // vm.salesData = regSalesData.regData;

        vm.SelectedCustomer={};
            
         vm.SelectedCustomer = openSalesData.salesData;
        
        console.log(JSON.stringify(vm.SelectedCustomer)+ " in change installer");
        vm.salesData={
        InstallerId:"",
            SalesId:vm.SelectedCustomer.SalesId
        };
        
        vm.agentList = [];
        
        agentDataService.getAllAgents().then(function (response) {
            vm.agentList = response.result[0].rows;
            console.log(JSON.stringify(vm.agentList));
        }, function (err) {

            notificationService.error('Network Error');
        });
        
        vm.updateInstaller = function(){
            customerSalesDataService.updateSalesInstaller(vm.salesData).then(function (result) {
                notificationService.success("Sales Assigned To Selected Agent");   
                
                vm={};
                openSalesData.salesData={};
                $state.go('app.assign-sales-installation');
        }, function (err) {
            notificationService.error('Error Agent Not Updated');
        });
        };
        
        
         var getLongLagFromAddress = function (address) {
            var deferred = $q.defer();
            var latlng = {
                lat: "",
                lng: ""
            };

            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({
                    'address': address
                },
                function (results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        deferred.resolve(results[0]);

                    } else {
                        //deferred.reject(results);
                    }


                });

            return deferred.promise;
        };


        vm.loadNearestAgentFieldMap = function () {
            var latlong = {};
            getLongLagFromAddress(openSalesData.salesData.Address).then(function (result) {

                    latlong.lat = result.geometry.location.lat();
                    regSalesData.regData.sales.long = result.geometry.location.lng();
                    regSalesData.regData.sales.latt = result.geometry.location.lat();

                    regSalesData.regData.sales.salesAgentId = authService.authentication.id;

                    $state.go('app.installers-location');

                },
                function (err) {
                    notificationService.error("Please Retry");
                }
            );


        };

        
        
        /*customerSalesDataService.getOpenInstallation().then(function (result) {
            vm.openInstallationList = result.result[0].rows;
            
            console.log(JSON.stringify(result.result[0].rows) + " in open List");
        }, function (err) {
            notificationService.error('Customers List Cannot Be Retrieved Now');
        });
        
        
        var createCustomerSales = function(){ 
        
        };
        
        vm.populateCustomerData = function (selectedCustomer)
        {
            
            openSalesData.salesData = selectedCustomer;
            console.log(JSON.stringify(openSalesData.salesData));
            $state.go('app.change-installer');
        };
*/
        
          
        return vm;
    }


})();
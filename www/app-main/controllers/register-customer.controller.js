(function () {

    'use strict';
    angular.module('sinet.controllers').controller('registerUserCtrl', Ctrl);
    Ctrl.$inject = ['$q', '$state', '$scope', 'authService', 'appSettingSevice', 'roleDataService', 'notificationService', 'localStorageService', 'addressDataService', 'regSalesData','mapDataResolve'];
    /* @ngInject */
    function Ctrl($q, $state, $scope, authService, appSettingService, roleDataService, notificationService, localStorageService, addressDataService, regSalesData,mapDataResolve) {
        var vm = $scope;

        vm.regData = regSalesData.regData;
        vm.roles = [];
        vm.states = [];
        vm.lgas = [];
        vm.customerRoleId = 0;
        var auth = localStorageService.get('authorizationData');
        vm.isAgent = authService.authentication.roles == 'SalesAgent';

        var authourised = authService.checkAuth();
        var latlng = {lat:"",lng:""};
        if (authourised === true) {
            roleDataService.getAll().then(function (response) {

                var roles = response.result[0].rows;
                console.log(authourised +" in registerUserCtrl line 16");

                vm.roles = roles;
                vm.customerRoleId = getCustomerRoleId(roles, 'Customer');
                vm.regData.roleId = vm.customerRoleId;

            }, function (err) {
                notificationService.error(err);
            });

            addressDataService.getSates().then(function (response) {
                vm.states = response;


            }, function (err) {

                notificationService.error('Connectivity Error');
            });


        } else {
            $state.go('app.auth-login');
            notificationService.error('Invalid Login Details');
        }

        vm.getStateLga = function (stateId) {
            addressDataService.getLgaByStateId(stateId).then(function (response) {


                vm.lgas = response;

            }, function (err) {

                notificationService.error('Connectivity Error');
            });

        };

        vm.addCustomerDetail = function () {
            //console.log(vm.regData.roleId+ " !== "+ vm.customerRoleId +" check role equality");
            if (vm.regData.roleId == vm.customerRoleId) {
                var selectedState = getNameById(vm.states, vm.regData.stateId);
                
                var selectedLga = getNameById(vm.lgas, vm.regData.lgaId);
                vm.regData.address = vm.regData.address + " " + selectedLga + " " + selectedState;
                
                vm.regData.sales.latt = mapDataResolve.Lat;
                vm.regData.sales.long = mapDataResolve.Long;
                
                
                $state.go('app.add-sales');
            } else {
                authService.createUser(vm.regData).then(function (success) {
                    vm.regData = {};
                    notificationService.success('User Successfully Created');
                }, function (err) {

                    notificationService.error('Network Error, Please Retry');
                });
            }
        };

        var getCustomerRoleId = function (a, obj) {
            for (var i = 0; i < a.length; i++) {
                if (a[i].Name == obj) {
                    return a[i].Id;
                }
            }
            return 0;
        };

        var getNameById = function (a, obj) {
            for (var i = 0; i < a.length; i++) {

                if (a[i].Id == obj) {
                    return a[i].Name;
                }
            }
            return 0;
        };


        vm.registerUser = function () {

            var userDetails = vm.regData;
            if (userDetails !== null) {
                notificationService.success('User Created');
            }

        };

        /*angular.extend(this, {

        });*/

        return vm;
    }


})();
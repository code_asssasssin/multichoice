(function() {
  'use strict';
  angular.module('sinet.controllers').controller('LoginCtrl', LoginCtrl);
  LoginCtrl.$inject = ['$q', '$scope', '$state', 'authService', 'notificationService', '$window'];


  function LoginCtrl($q, $scope, state, authService, notificationService, $window) {
    var vm = $scope;

    vm.loginData = {
      "username": "",
      "password": ""
    };

    /* vm.close = function () {
         state.go('app.home');
     };*/

    vm.login = function() {
      authService.login(this.loginData)
        .then(function(response) {
          var roles = [];
          roles = response.Roles.toLowerCase().split(',');
          $scope.$emit('logged-in', true);
          if (roles.length >= 1) {
            notificationService.success('Log In Success');

            if (contains(roles, 'admin') === true) {
              state.go('app.register-user');
            } else if (contains(roles, 'installer') === true) {
              state.go('app.assigned-jobs');
            } else if (contains(roles, 'salesagent') === true) {
              state.go('app.register-customer');
            }
          } else {
            notificationService.error("Invalid Username or Password");
            state.go('app.register-user');
          }

        }, function(err) {
          notificationService.error("Invalid Username or Password");
          state.go('app.register-user');
        });
    };

    var contains = function(a, obj) {
      for (var i = 0; i < a.length; i++) {
        if (a[i] === obj) {
          return true;
        }
      }
      return false;
    };

    return vm;
  }

})();

(function () {

    'use strict';
    angular.module('sinet.controllers').controller('custListCtrl', Ctrl);
    Ctrl.$inject = ['$q', '$state', '$scope', 'authService', 'appSettingSevice', 'customerSalesDataService', 'regSalesData','notificationService'];
    /* @ngInject */
    function Ctrl($q, $state, $scope, authService, appSettingService, customerSalesDataService, regSalesData,notificationService) {
        var vm = $scope;

        vm.customerSalesData = regSalesData.regData.sales;

        vm.customersList = [];
        
        var selectedCustomer = {};

        
        customerSalesDataService.getCustomers().then(function (result) {
            vm.customersList = result.data;
        }, function (err) {
            notificationService.error('Customers List Cannot Be Retrieved Now');
        });
        
        
        var createCustomerSales = function(){ 
        
        };
        
        vm.populateCustomerData = function (selectedCustomer)
        {
            
            vm.customerSalesData.salesAgentId = authService.authentication.id;
            vm.customerSalesData.address = selectedCustomer.Address;
            vm.customerSalesData.contactPhone = selectedCustomer.PhoneNumber;
            vm.customerSalesData.customerId = selectedCustomer.Id;
            
            
            $state.go('app.add-customer-sales');
        };

        
          
        return vm;
    }


})();
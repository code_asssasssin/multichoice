(function() {

    'use strict';
    angular.module('sinet.controllers').controller('orderCtrl', Ctrl);
    Ctrl.$inject = ['$q', '$state', '$scope', 'authService', 'appSettingSevice', 'roleDataService', 'notificationService', 'localStorageService', 'productDataService', 'agentDataService', 'regSalesData', 'customerSalesDataService', 'assignedJobsDataService'];
    /* @ngInject */
    function Ctrl($q, $state, $scope, authService, appSettingService, roleDataService, notificationService, localStorageService, productDataService, agentDataService, regSalesData, customerSalesDataService, assignedJobsDataService) {
      var vm = $scope;

      vm.salesData = regSalesData.regData;
      vm.productList = [];
      var products = [];


      productDataService.getAllProducts().then(function(result) {

        vm.productList = result;

      }, function(err) {

        notificationService.error('Network Error');

      });

      agentDataService.getAllAgents().then(function(response) {
        vm.agentList = response.result[0].rows;
        console.log(JSON.stringify(vm.agentList));
      }, function(err) {

        notificationService.error('Network Error');
      });


      vm.createOrder = function() {


        var latlng = {
          lat: "",
          lng: ""
        };
        var customerId = vm.salesData.sales.customerId;
        assignedJobsDataService.getCustomerCoordinate(customerId).then(function(response) {
            latlng.lat = response.Latitude;
            latlng.lng = response.Longitude;
            console.log(JSON.stringify(latlng.lat) + " in add-order Controller line 44");
            addSales(latlng);
          }, function(err) {}

        );

      };

      var addSales = function(latlng) {
        var salesId = "";
        var data = {};

        customerSalesDataService.addSales(vm.salesData.sales, latlng).then(function(response) {

          salesId = response.procresults[0].result[0].rows[0].SalesOrderID;
          data = {
            SalesId: salesId,
            ProductId: vm.salesData.sales.salesProduct.productId,
          };

          if (data.SalesId != "" && data.ProductId != "") {
            notificationService.success(salesId);

            productDataService.addSalesProduct(data).then(function(response) {
              notificationService.success("Sales Created");
              $state.reload();
              $state.go('app.customer-list');
            }, function(err) {

            });

          }

        }, function(err) {});
      };

      return vm;


    }

  }

)();

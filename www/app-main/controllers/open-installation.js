(function () {

    'use strict';
    angular.module('sinet.controllers').controller('openInstallationCtrl', Ctrl);
    Ctrl.$inject = ['$q', '$state', '$scope', 'authService', 'appSettingSevice', 'customerSalesDataService', 'notificationService', 'openSalesData'];
    /* @ngInject */
    function Ctrl($q, $state, $scope, authService, appSettingService, customerSalesDataService, notificationService, openSalesData) {
        var vm = $scope;
        //  vm.customerSalesData = regSalesData.regData.sales;

        vm.openInstallationList = [];

        var selectedInstallation = {};
        //vm.selectedSales = {};

        customerSalesDataService.getOpenInstallation().then(function (result) {
            vm.openInstallationList = result.result[0].rows;

        }, function (err) {
            notificationService.error('Customers List Cannot Be Retrieved Now');
        });

       
        vm.populateCustomerData = function (selectedCustomer) {

            openSalesData.salesData = selectedCustomer;
            console.log(JSON.stringify(openSalesData.salesData));
            $state.go('app.change-installer');
        };



        return vm;
    }


})();
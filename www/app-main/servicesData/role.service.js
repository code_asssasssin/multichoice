(function () {
    'use strict';
    angular.module('sinet.services').factory('roleDataService', roleService);
    roleService.$inject = ['$q', '$http', 'appSettingSevice'];
    /* @ngInject */
    function roleService($q, $http, appSettingSevice) {
        /**************************************************/
        var service = this;
        var apiUrlBase = appSettingSevice.apiServiceBaseUri;

        var routePrefix = "main:AllRole/";
        var urlApiPrefix = apiUrlBase + routePrefix;
        //-----------------------------------
        service.getAll = function () {
            var deferred = $q.defer();
            $http({
                method: 'get',
                url: urlApiPrefix
            }).then(function (result) {
                
                deferred.resolve(result.data);
            }, function (result) 
            {
                deferred.reject(result.data);
            });
            return deferred.promise;
        };

        service.getById = function (dataId) {
            var deferred = $q.defer();
            $http({
                method: 'get',
                url: urlApiPrefix + ''
            }).then(function (result) {
                
                deferred.resolve(result.data);
            }, function (result) {
                deferred.reject(result.data);
            });
            return deferred.promise;
        };
        //------------------------------------------
        return service;
        /******************************************/
    }
})();
(function () {
    'use strict';
    angular.module('sinet.services').factory('_xxDataService', Profile);
    Profile.$inject = ['$q', '$http', 'appSettingSevice'];
    /* @ngInject */
    function Profile($q, $http, appSettingSevice) {
        /**************************************************/
        var service = this;
        var apiUrlBase = appSettingSevice.apiServiceBaseUri;

        //////////////////////////////////
        // APIs 
        var routePrefix = "profile/";
        var urlApiPrefix = apiUrlBase + routePrefix;
        //-----------------------------------

        service.getAll = function () {
            var deferred = $q.defer();
            $http({
                method: 'get',
                url: urlApiPrefix + ''
            }).then(function (result) {
                //console.log(result.data);
                deferred.resolve(result.data);
            }, function (result) {
                deferred.reject(result.data);
            });
            return deferred.promise;
        };

        service.getById = function (dataId) {
            var deferred = $q.defer();
            $http({
                method: 'get',
                url: urlApiPrefix + ''
            }).then(function (result) {
                //console.log(result.data);
                deferred.resolve(result.data);
            }, function (result) {
                deferred.reject(result.data);
            });
            return deferred.promise;
        };

        service.save = function (formData) {
            var deferred = $q.defer();
            $http({
                method: 'post',
                url: urlApiPrefix + '',
                data: formData
            }).then(function (result) {
                //console.log(result.data);
                deferred.resolve(result.data);
            }, function (result) {
                deferred.reject(result.data);
            });
            return deferred.promise;
        };
        //------------------------------------------
        return service;
        /******************************************/
    }
})();
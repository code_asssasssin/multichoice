(function() {
  'use strict';
  angular.module('sinet.services').factory('regSalesData', regSalesData);

  function regSalesData() {
    var vm = this;


    vm.regData = {
      firstname: "",
      lastname: "",
      username: "",
      stateId: "",
      city: "",
      address: "",
      comment: "",
      roleId: "",
      lgaId: 1,
      password: "",
      cpasswpord: "",
      email: "",
      contactPhone: "",
      sales: {
        salesAgentId: "",
        installerId: null,
        customerId: "",
        address: "",
        contactPhone: "",
        long: "",
        latt: "",
        orderDate: "",
        fulfillmentDate: "",
        salesProduct: {
          productId: "",
          productName: "",
          quantity: "",
          salesId: ""

        }

      }
    };
      
    


    return vm;
  }
})();

(function () {
    'use strict';
    angular.module('sinet.services').factory('assignedJobsDataService', assignedJobsService);
    assignedJobsService.$inject = ['$q', '$http', 'appSettingSevice'];
    /* @ngInject */
    function assignedJobsService($q, $http, appSettingSevice) {
        /**************************************************/
        var service = this;
        var apiUrlBase = appSettingSevice.apiServiceBaseUri;

        var routePrefix = "Sales";
        var urlApiPrefix = apiUrlBase + routePrefix;
        //-----------------------------------
        service.getAllJobs = function (id) {
            var deferred = $q.defer();
            $http({
                method: 'get',
                url: urlApiPrefix + "?filter=InstallerId='" + id + "'"
            }).then(function (result) {

                deferred.resolve(result.data);
            }, function (result)
            {
                deferred.reject(result.data);
            });
            return deferred.promise;
        };

        service.getCustomerCoordinate = function (id) {
            var deferred = $q.defer();
            $http({
                method: 'get',
                url: apiUrlBase + "GetCustomerCoordinate?arg.customerId=" + id
            }).then(function (result) {
                console.log(result);
                deferred.resolve(result.data.result[0].rows[0]);
            }, function (result)
            {
                deferred.reject(result.data);
            });
            return deferred.promise;
        };

        service.updateJobStatus = function(Status){
            var deferred = $q.defer();
            $http({
                method: 'post',
                url: apiUrlBase + 'UpdateSales' ,
                data: Status
            }).then(function (result) {
                console.log(result);
                deferred.resolve(result.data);
            }, function (result)
            {
                deferred.reject(result.data);
            });
            return deferred.promise;
        }

        //------------------------------------------
        return service;
        /******************************************/
    }
})();
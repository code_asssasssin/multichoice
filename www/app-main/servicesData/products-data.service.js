(function () {
    'use strict';
    angular.module('sinet.services').factory('productDataService', productService);
    productService.$inject = ['$q', '$http', 'appSettingSevice'];
    /* @ngInject */
    function productService($q, $http, appSettingSevice) {
        /**************************************************/
        var service = this;
        var apiUrlBase = appSettingSevice.apiServiceBaseUri;

        var routePrefix = "main:Products/";
        var createSalesProductPrefix = "main%3ASalesProducts/";
        var urlApiPrefix = apiUrlBase + routePrefix;
        var createSalesProductUrl = apiUrlBase +createSalesProductPrefix;
        //-----------------------------------
        service.getAllProducts = function () {
            var deferred = $q.defer();
            $http({
                method: 'get',
                url: urlApiPrefix
            }).then(function (result) {
                
                deferred.resolve(result.data);
            }, function (result) 
            {
                deferred.reject(result.data);
            });
            return deferred.promise;
        };
        
          service.addSalesProduct = function (data) {
            var deferred = $q.defer();
              
              console.log(JSON.stringify(data) + " data inside product service");
            $http({
                method: 'post',
                url: createSalesProductUrl,
                data:data
            }).then(function (result) {
                
                deferred.resolve(result.data);
            }, function (result) 
            {
                deferred.reject(result.data);
            });
            return deferred.promise;
        };

        
        //------------------------------------------
        return service;
        /******************************************/
    }
})();
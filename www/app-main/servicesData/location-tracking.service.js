(function () {
    'use strict';
    angular.module('sinet.services').service('trackingDataService', trackingService);
    trackingService.$inject = ['$q', '$http', 'appSettingSevice'];
    /* @ngInject */
    function trackingService($q, $http, appSettingSevice) {
        /**************************************************/
        var service = this;
        var apiUrlBase = appSettingSevice.apiServiceBaseUri;

        var routePrefix = "main:AddTracking/";
        var urlApiPrefix = apiUrlBase + routePrefix;
        //-----------------------------------
        service.addTracking = function (trackData) {
            var deferred = $q.defer();
            $http({
                method: 'post',
                url: urlApiPrefix,
                data: trackData
            }).then(function (result) {
                console.log(result);
                deferred.resolve(result);
            }, function (result)
            {
                deferred.reject(result.data);
            });
            return deferred.promise;
        };


        //------------------------------------------
        return service;
        /******************************************/
    }
})();
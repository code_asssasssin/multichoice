(function () {
    'use strict';
    angular.module('sinet.services').factory('agentDataService', agentService);
    agentService.$inject = ['$q', '$http', 'appSettingSevice'];
    /* @ngInject */
    function agentService($q, $http, appSettingSevice) {
        /**************************************************/
        var service = this;
        var apiUrlBase = appSettingSevice.apiServiceBaseUri;

        var routePrefix = "main:Agents/";
        var urlApiPrefix = apiUrlBase + routePrefix;
        //-----------------------------------
        service.getAllAgents = function () {
            var deferred = $q.defer();
            $http({
                method: 'get',
                url: urlApiPrefix
            }).then(function (result) {
                
                deferred.resolve(result.data);
            }, function (result) 
            {
                deferred.reject(result.data);
            });
            return deferred.promise;
        };

        
        //------------------------------------------
        return service;
        /******************************************/
    }
})();
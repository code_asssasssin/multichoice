(function () {
    'use strict';
    angular.module('sinet.services').factory('addressDataService', addressService);
    addressService.$inject = ['$q', '$http', 'appSettingSevice'];
    /* @ngInject */
    function addressService($q, $http, appSettingSevice) {
        /**************************************************/
        var service = this;
        var apiUrlBase = appSettingSevice.apiServiceBaseUri;

        var lgaPrefix = "main:LGA/";
        var statePrefix = "main:State/";

        var lgaUrlApiPrefix = apiUrlBase + lgaPrefix;
        var stateUrlApiPrefix = apiUrlBase + statePrefix;

        
        service.getSates = function () {
            var deferred = $q.defer();
            $http({
                method: 'get',
                url: stateUrlApiPrefix
            }).then(function (result) {
                
                deferred.resolve(result.data);
            }, function (result) 
            {
                deferred.reject(result.data);
            });
            return deferred.promise;
        };

        service.getLgaByStateId = function (dataId) {
            var deferred = $q.defer();
            $http({
                method: 'get',
                url: lgaUrlApiPrefix + '?filter=stateid='+dataId
            }).then(function (result) {
                
                deferred.resolve(result.data);
            }, function (result) {
                deferred.reject(result.data);
            });
            return deferred.promise;
        };
        //------------------------------------------
        return service;
        /******************************************/
    }
})();
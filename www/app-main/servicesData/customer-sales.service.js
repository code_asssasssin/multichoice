(function () {
    'use strict';
    angular.module('sinet.services').factory('customerSalesDataService', customerSalesDataService);
    customerSalesDataService.$inject = ['$q', '$http', 'appSettingSevice'];
    /* @ngInject */
    function customerSalesDataService($q, $http, appSettingSevice) {
        /**************************************************/
        var service = this;
        var apiUrlBase = appSettingSevice.apiServiceBaseUri;


        var routePrefix = "main:CreateCustomernSales/";
        var customersPrefix = "main:Customer/";
        var createSaleRoutePrefix ="main:CreateSale/";
        var getOpenInstallationRoutePrefix = "main:OpenInstallations/";
        var updateSalesInstallerPrefix = "main:ChangeInstaller";
        
        
        var customersResourceUrl = apiUrlBase+customersPrefix;
        var urlApiPrefix = apiUrlBase+routePrefix;
        var createSalesUrl = apiUrlBase+createSaleRoutePrefix;
        var OpenInstallationsUrl = apiUrlBase+getOpenInstallationRoutePrefix;
        var changeInstallerUrl = apiUrlBase+updateSalesInstallerPrefix;
        //-----------------------------------
        service.createAgentAndSales = function (salesData) {
            var deferred = $q.defer();

            var reqDate = new Date();
            reqDate = reqDate.toISOString();

            var createCustomernSalesData = {
                SalesAgentID: salesData.sales.salesAgentId,
                InstallerID: salesData.sales.installerId,
                RequestDate: reqDate,
                StatusID: 4,
                Longitude: salesData.sales.long,
                Latitude: salesData.sales.latt,
                ContactPhone: salesData.contactPhone,
                SalesTotal: 1000,
                CustomerFName: salesData.firstname,
                CustomerLName: salesData.lastname,
                Email: salesData.email,
                PhoneNumber: salesData.contactPhone,
                Address: salesData.address,
                Comment: salesData.comment,
                LgaId: salesData.lgaId
            };
            


            $http({
                method: 'post',
                url: urlApiPrefix,
                data: createCustomernSalesData
            }).then(function (result) {

                deferred.resolve(result.data);
            }, function (result) {
                deferred.reject(result.data);
            });
            return deferred.promise;
        };


        service.getCustomers = function () {
            var deferred = $q.defer();
            $http({
                method: 'get',
                url: customersResourceUrl
            }).then(function (result) {
                deferred.resolve(result);
            }, function (err) {
                deferred.reject(err);
                
            });

            return deferred.promise;
        };
        
        service.addSales = function(salesData,latlng){
            
             var reqDate = new Date();
            reqDate = reqDate.toISOString();
            
            var salesSPParams = {
                SalesAgentID: salesData.salesAgentId,
                CustomerID: salesData.customerId,
                InstallerID:salesData.installerId,
                RequestDate: reqDate,
                StatusID: 4,
                SaleAmount: 1000,
                Comment: "Deliver ASAP",
                Longitude: latlng.lng,
                Latitude:latlng.lat,
                ContactPhone: salesData.contactPhone,
	            ContactAddress: salesData.address,
	            SalesTotal: "1000"
            };
            
             var deferred = $q.defer();
            $http({
                method: 'post',
                url: createSalesUrl,
                data:salesSPParams
            }).then(function (result) {
                deferred.resolve(result.data);
            }, function (err) {
                deferred.reject(err);
                console.log(JSON.stringify(err));
                alert("error");
            });

            return deferred.promise;
        
        };
        
        
         service.updateSalesInstaller = function(salesData){
            console.log(JSON.stringify(salesData)+ " in updateSalesData");
           
             var deferred = $q.defer();
            $http({
                method: 'post',
                url: changeInstallerUrl,
                data:salesData
            }).then(function (result) {
                deferred.resolve(result.data);
            }, function (err) {
                deferred.reject(err);
                console.log(JSON.stringify(err));
                alert("error");
            });

            return deferred.promise;
        
        };
        
        
         service.getOpenInstallation = function () {
            var deferred = $q.defer();
            $http({
                method: 'get',
                url: OpenInstallationsUrl
            }).then(function (result) {
                deferred.resolve(result.data);
            }, function (err) {
                deferred.reject(err);
                
            });

            return deferred.promise;
        };
        
        

        //------------------------------------------
        return service;
        /******************************************/
    }
})();
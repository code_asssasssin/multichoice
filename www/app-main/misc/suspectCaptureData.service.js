(function () {
    'use strict';
    angular.module('sinet.services').factory('regData', regData);

    function regData() {
        var vm = this;
     
        
         vm.suspectData = {
            fname: "",
            lname: "",
            bDate: "",
            idType: "",
            idNumber: "",
            offenceType: "",
            loc: "",
            notes: "",
            imageData: "",
            thumbData: "",
            id: "",
            address: "",
            vehicleType: "",
            vehicleModel: "",
            vehicleMake: "",
            vImageData: "",
            chasisNo: "",
            engineNo: "",
            tagNumber: ""

        };

        vm.guid = function () {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        };

        return vm;
    };
})();
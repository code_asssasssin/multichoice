(function () {
    'use strict';
    angular.module('sinet.controllers').controller('suspectCtrl', Ctrl);
    Ctrl.$inject = ['$q', '$state', '$scope', 'authService', 'suspectService', 'regData'];
    /* @ngInject */
    function Ctrl($q, $state, $scope, authService, suspectService, registrationData) {
        var vm = $scope;

        vm.regData = registrationData.suspectData;

        vm.register = function () {
          //  $state.go('app.suspect-picture');
            
            $state.go('app.suspect-vehicle');
        };

        function takepicture() {
            if (authService.checkAuth) {
                $state.go('app.suspect-vehicle');
            } 
            else {
                loginModal();
            }
        }

        function capturePrint() {
            suspectService.suspectRecordKey(this.regData);
        }

        function loginModal() {
            modalDialogService.show(viewDir + "auth-login-modal.html");
        }

        function modalhide() {
            modalDialogService.hide();
        }

        angular.extend(this, {
            takepicture: takepicture,
            capturePrint: capturePrint,
            loginModal: loginModal,
            modalhide: modalhide
        });

        return vm;
    }
})();
(function () {
    'use strict';
    angular.module('sinet.controllers').controller('suspectThumbPrintCtrl', Ctrl);
    Ctrl.$inject = ['$q', '$state', '$scope', 'regData', 'localStorageService', 'localStorageKeyService', 'notificationService'];
    /* @ngInject */
    function Ctrl($q, $state, $scope, registrationData, localStorageService, localStorageKeyService, notificationService) {
        $scope.imageData = {};
        $scope.regData = registrationData.suspectData;
        $scope.imageSrcDisplay = "img/fingerprint.jpg";
        $scope.disableUpload = true;
        document.addEventListener("deviceready", function () {
            $scope.upload = function () {
                localStorageService.set(localStorageKeyService.suspectDataKey, $scope.regData);
                $scope.regData = {};
                registrationData.suspectData = {};
                notificationService.success('Suspect Record Created');
                
                $state.go('app.suspect-register');
            };

            $scope.uploadCapture = function () {
                var options = {
                    quality: 80,
                    destinationType: Camera.DestinationType.DATA_URL,
                    //destinationType: Camera.DestinationType.FILE_URI,
                    sourceType: 1, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
                    //sourceType: Camera.PictureSourceType.CAMERA,
                    allowEdit: false,
                    //encodingType: Camera.EncodingType.JPEG,
                    encodingType: 0, // 0=JPG 1=PNG
                    targetWidth: 100,
                    targetHeight: 100,
                    saveToPhotoAlbum: false,
                    correctOrientation: true
                };

                navigator.camera.getPicture(onSuccess, onFail, options);
            };

            var onSuccess = function (imgData) {
                //console.log(FILE_URI);
                var img = "data:image/jpeg;base64," + imgData;

                if (imgData) {
                    $scope.imageSrcDisplay = img || 'img/user-icon.png';
                    $scope.disableUpload = false;
                }

                $scope.imageData = imgData;
                $scope.regData.thumbData = imgData;

                $scope.$apply();
            };

            var onFail = function (e) {
                //console.log("On fail " + e);
            };

            $scope.send = function () {
                var myImg = $scope.picData;
                var options = new FileUploadOptions();
                options.fileKey = "post";
                options.chunkedMode = false;
                var params = {};
                params.user_token = localStorage.getItem('auth_token');
                params.user_email = localStorage.getItem('email');
                options.params = params;
                var ft = new FileTransfer();
                ft.upload(myImg, encodeURI("https://example.com/posts/"), onUploadSuccess, onUploadFail, options);
            }
        }, false);
    }
})();
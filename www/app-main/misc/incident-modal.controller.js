(function () {
    'use strict';
    angular.module('sinet.controllers').controller('modalMapCtrl', Ctrl);
    Ctrl.$inject = ['$q', '$state', '$scope', 'modalDialogService', 'appSettingSevice', 'uiGmapGoogleMapApi', 'listData'];
    /* @ngInject */
    function Ctrl($q, $state, $scope, modalDialogService, appSettingSevice, uiGmapGoogleMapApi, listData) {

        var vm = $scope;
        vm.randomMarkers = listData.randomData.randomMarkers;

        console.log(listData.randomData.randomMarkers);
        console.log(listData.randomData.map);
        var latVal = listData.randomData.randomMarkers[3].latitude,
            longVal = listData.randomData.randomMarkers[3].longitude;

        uiGmapGoogleMapApi.then(function (maps) {
            vm.map = listData.randomData.map;
        });


        vm.options = {
            scrollwheel: true
        };


        vm.incidentModalHide = function () {
            modalDialogService.hide();
        };

        angular.extend(this, {
            /* gotoUrl: gotoUrl,
             setReportData: setReportData*/
        });

        return vm;
    }
})();
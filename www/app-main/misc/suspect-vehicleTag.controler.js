(function () {
    'use strict';
    angular.module('sinet.controllers').controller('suspectVehicleTagCtrl', Ctrl);
    Ctrl.$inject = ['$q', '$state', '$scope', 'regData', 'localStorageService','localStorageKeyService','notificationService'];
    function Ctrl($q, $state, $scope, registrationData,localStorageService,localStorageKeyService,notificationService) {
        var scp = $scope;

        scp.regData = registrationData.suspectData;

        console.log(scp.regData);

        scp.imageData = {};
        scp.imageSrcDisplay = "img/user-icon.png";
        scp.disableUpload = true;

        document.addEventListener("deviceready", function () {

            scp.upload = function () {
                $state.go('app.suspect-thumbprint');
            };
            scp.uploadCapture = function () {

                var options = {
                    quality: 40,
                    destinationType: Camera.DestinationType.DATA_URL,
                    //destinationType: Camera.DestinationType.FILE_URI,
                    sourceType: 1, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
                    //sourceType: Camera.PictureSourceType.CAMERA,
                    allowEdit: false,
                    //encodingType: Camera.EncodingType.JPEG,
                    encodingType: 0, // 0=JPG 1=PNG
                    targetWidth: 100,
                    targetHeight: 100,
                    //popoverOptions: CameraPopoverOptions,//iOS-only options that specify popover location in iPad
                    saveToPhotoAlbum: false,
                    correctOrientation: true
                };


                navigator.camera.getPicture(onSuccess, onFail, options);
            };

            function onSuccess(imgData) {
                var img = "data:image/jpeg;base64," + imgData;

                if (imgData) {
                    scp.imageSrcDisplay = img || 'img/user-icon.png';
                    scp.disableUpload = false;
                }

                scp.imageData = imgData;

                scp.regData.vImageData = img;
                var uuid = registrationData.guid();
                scp.regData.id = uuid;
                //localStorageService.set(localStorageKeyService.suspectDataKey, scp.regData);

                scp.$apply();
            }

            function onFail(e) {
                //console.log("On fail " + e);
            }

            /*scp.send = function () {
                var myImg = scp.picData;
                var options = new FileUploadOptions();
                options.fileKey = "post";
                options.chunkedMode = false;
                var params = {};
                params.user_token = localStorage.getItem('auth_token');
                params.user_email = localStorage.getItem('email');
                options.params = params;
                var ft = new FileTransfer();
                ft.upload(myImg, encodeURI("https://example.com/posts/"), onUploadSuccess, onUploadFail, options);
            };*/
        }, false);
    }
})();


(function () {
    'use strict';
    angular.module('sinet.controllers').controller('menuCtrl', menuCtrl);
    menuCtrl.$inject = ['$q', '$state', 'menuService', 'authService', 'localStorageService', 'modalDialogService', 'appSettingSevice'];
    /* @ngInject */
    function menuCtrl($q, $state, menuService, authService, localStorageService, modalDialogService, appSettingSevice) {
        var menuData = menuService.menu();
        var viewDir = appSettingSevice.viewDir;

        function gotoUrl(urlState) {
            $state.go(urlState);
        }

        function loginModal1() {
            modalDialogService.show(viewDir + "auth-signup-modal.html");
        }

        function loginModal2() {
            modalDialogService.show(viewDir + "auth-login-modal.html");
        }

        function login() {
            $state.go('app.auth-login');
        }

        function modalhide() {
            modalDialogService.hide();
        }

        angular.extend(this, {
            menuData: menuData,
            gotoUrl: gotoUrl,
            loginModal1: loginModal1,
            loginModal2: loginModal2,
            modalhide: modalhide,
            login: login
        });
    }
})();
(function () {
    'use strict';
    angular.module('sinet.controllers').controller('incidentListCtrl', Ctrl);
    Ctrl.$inject = ['$q', '$state', '$scope','localStorageService', 'localStorageKeyService', 'modalDialogService','appSettingSevice','uiGmapGoogleMapApi'];
    /* @ngInject */
    function Ctrl($q, $state, $scope,localStorageService,localStorageKeyService,modalDialogService,appSettingSevice, uiGmapGoogleMapApi) {

        var vm = this;

        $scope.map = {};

        $scope.options = {
            scrollwheel: true
        };
        $scope.randomMarkers = [];
        /*start*/
        var lat = 6.453055600000000000,
            lng = 3.395833300000049300;

        uiGmapGoogleMapApi.then(function (maps) {
            $scope.map = {
                center: {
                    latitude: lat,
                    longitude: lng
                },
                zoom: 16,
                bounds: {}

            };
        });

       
        
         vm.createRandomMarker = function (i, bounds, idKey, lat, lng) {
        
        console.log('creating random data');

            if (idKey === null) {
                idKey = "id";
            }



            var latitude = lat + (Math.random() * 0.0065600000000000);
            var longitude = lng + (Math.random() * 0.0065833300000049300);
            var ret = {};
            if (i === 3) {
                ret = {
                    latitude: latitude,
                    longitude: latitude,
                    title: 'm' + i,
                    id: i,
                    icon: './img/policemanCentre.png'

                };

                /*$scope.map = {
                    center: {
                        latitude: latitude,
                        longitude: latitude
                    },
                    zoom:16
                };*/

                return ret;

            } else {
                ret = {
                    latitude: latitude,
                    longitude: longitude,
                    title: 'm' + i,
                    id: i,
                    icon: 'img/policeman.png'

                };
                //ret[idKey] = i;
                return ret;
            }
        };
        
        vm.setReportData = function () {
                console.log('setting');
            var markers = [];
            for (var i = 0; i < 6; i++) {
                markers.push(this.createRandomMarker(i, $scope.map.bounds, null, lat, lng));
            }
            
            
            $scope.randomMarkers = markers;
          
        };

       

      
        
        this.setReportData(); 
        
        
         uiGmapGoogleMapApi.then(function (maps) {
            $scope.map = {
                center: {
                    latitude: lat,
                    longitude: lng
                },
                zoom: 16,
                bounds: {}

            };
             
             console.log('maplat ' + lat);
        });


        var viewDir = appSettingSevice.viewDir;

        vm.reportObj = {
            lat: "",
            lng: "",
            incidenceType: "",
            description: "",
            image: "",
            video: "",
            address: "",
            id: ""
        };



        vm.reportedIncidentDetails = function () {
            console.log('here');
            var storedIncidents = localStorageService.get(localStorageKeyService.incidentReportkey);
            return storedIncidents;
        };


        function gotoUrl(urlState) {
            $state.go(urlState);
        }

        $scope.incidentModalShow = function (obj) {



            modalDialogService.show(viewDir + "reported-incident-map-modal.html", $scope.randomMarkers);


        };

        $scope.getImageIcon = function (imageData) {
            var img = imageData;
            return img;
        };




        $scope.incidentModalHide = function () {
            modalDialogService.hide();
        };

        angular.extend(this, {
            gotoUrl: gotoUrl,
            // setReportData: setReportData
        });

        $scope.storedIncidents = this.reportedIncidentDetails();


        return vm;
    }
})();
(function () {
    'use strict';
    angular.module('sinet.controllers').controller('testCtrl', Ctrl);
    Ctrl.$inject = ['$q', '$state', 'localStorageService', 'utilityService'];
    /* @ngInject */
    function Ctrl($q, $state, localStorageService, utilityService) {
        var vm = this;
        var storeKey = 'teststor333';
        vm.storageDataset = [];
        vm.data = [];
        var dataCount = 0;

        function genData(n) {
            dataCount = n;
            console.log("data generated count: ", n);
            // console.log(n);

            function mdata() {
                var nx = Math.floor(Math.random() * 1040609);
                return {
                    "id": utilityService.getGui(),
                    "name": "Godwin_" + nx,
                    "number": nx
                };
            };

            for (var i = 0; n > i; i++) {
                vm.data.push(mdata());
            }
        };

        function setStorage() {

            var i = function () {
                return Math.floor(Math.random() * dataCount)
            };
            var dt = vm.data[i()];

            var dt2 = [dt];
            dt2.push(vm.data[i()]);
            dt2.push(vm.data[i()]);
            dt2.push(vm.data[i()]);
            //dt2.push(vm.data[i()]);

            localStorageService.append(storeKey, dt2, 'id');
        };

        function getStorage() {
            var d = localStorageService.get(storeKey);
            d = _.sortBy(d, 'id');
            //console.log(d);
            return d;
        };

        function appenToStorage() {
            setStorage();
            vm.storageDataset = getStorage();
            //console.log(vm.storageDataset);
        };

        function removeFromStorage() {
            localStorageService.remove(storeKey);
            vm.storageDataset = getStorage();
            console.log(vm.storageDataset);
        };

        function clearData() {
            vm.data = [];
            console.log(vm.data);
        };

        angular.extend(this, {
            setStorage: setStorage,
            getStorage: getStorage,
            appenToStorage: appenToStorage,
            genData: genData,
            clearData: clearData,
            removeFromStorage: removeFromStorage
        });
    }
})();
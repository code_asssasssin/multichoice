(function () {
    'use strict';
    angular.module('sinet.services').factory('listData', listData);

    function listData() {
        var vm = this;


        vm.randomData = {
            randomMarkers:[{}],
            map:{}
        };

        //vm.map={};

        var createRandomMarker = function (i, bounds, idKey, lat, lng) {

            if (idKey === null) {
                idKey = "id";
            }

            var latitude = lat + (Math.random() * lat);
            var longitude = lng + (Math.random() * lng);
            var ret = {};
            if (i === 3) 
            {
                ret = {
                    latitude: latitude,
                    longitude: latitude,
                    title: 'm' + i,
                    id: i,
                    icon: "img/policemanCentre.png"
                };

                vm.randomData.map = {
                    center: {
                        latitude: latitude,
                        longitude: latitude
                    }
                };
                
                


            } 
            else {
                ret = {
                    latitude: latitude,
                    longitude: longitude,
                    title: 'm' + i,
                    id: i,
                    icon: "img/policeman.png"

                };
            }
            
            vm.randomData.randomMarkers.push(ret);
        };
        
        var setReportData = function (obj) {

            var markers = [];
            for (var i = 0; i < 6; i++) {
                createRandomMarker(i, null, obj.lat, obj.lat);
            }
            
        };

       

        return vm;
    }
})();
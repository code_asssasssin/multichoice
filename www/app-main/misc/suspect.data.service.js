(function () {
        'use strict';
        angular.module('sinet.services').factory('suspectService', suspectService);
        suspectService.$inject = ['$http', '$q', '$state', 'localStorageService','localStorageKeyService', 'modalDialogService', 'appSettingSevice', 'notificationService'];

        function suspectService($http, $q, $state, localStorageService,localStorageKeyService,modalDialogService, appSettingSevice, notification) {
            var vm = this;
            var apiUrlBase = appSettingSevice.apiServiceBaseUri;
            var suspectRecordKey = localStorageKeyService.suspectDataKey;
            var views_baseDir = appSettingSevice.viewDir;

            function setSuspectRecord(suspectRecord) 
            {
                var suspectRecords = getSuspectRecord(suspectRecordKey);
                
                suspectRecords.push(suspectRecord);

                localStorageService.set(sus);
            }

            function getSuspectRecord() {
                return localStorageService.get(suspectRecordKey);
            }

            function checkSuspectImage(suspectRecord) {
                if (suspectRecord.image !== null && suspectRecord.image !== "")
                    return true;
                return false;
            }

            function checkSuspectRecord() {
                var susRecord = getSuspectRecord();
                if (suspectRecord !== null)
                    return true;
                notification.error("Register Suspect");
                return false;
            }

            function saveSuspectRecord() {
                var deferred = $q.defer();
                var susRecord = getSuspectRecord();
                var imageTaken = checkSuspectImage(susRecord);
                if (imageTaken) {
                    $http({
                        method: 'post',
                        url: urlApi_Prefix + 'addSuspect',
                        data: susRecord
                    }).then(function (response) {
                        console.log(response);
                        console.log(response.data);
                        deferred.resolve(response.data);
                    }, function (err) {
                        deferred.reject(err);
                    });
                    return deferred.promise;
                }

            }

            angular.extend(this, {
                setSuspectRecord: setSuspectRecord,
                getSuspectRecord: getSuspectRecord,
                saveSuspectRecord: saveSuspectRecord,
                checkSuspectRecord: checkSuspectRecord
            });
            
            return vm;
        };
    }
)();
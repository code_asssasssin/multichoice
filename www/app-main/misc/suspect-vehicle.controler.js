(function () {
    'use strict';
    angular.module('sinet.controllers').controller('vehicleCtrl', Ctrl);
    Ctrl.$inject = ['$q', '$state', '$scope', 'authService', 'suspectService', 'regData', 'localStorageService', 'localStorageKeyService'];
    /* @ngInject */
    function Ctrl($q, $state, $scope, authService, suspectService, registrationData) {
        var vm = $scope;

        vm.regData = registrationData.suspectData;

        vm.registerVehicle = function () {
           
            $state.go('app.suspect-picture');
        };

        angular.extend(this, {

        });

        return vm;
    }
})();
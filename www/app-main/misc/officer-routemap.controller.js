(function () {
    'use strict';
    angular.module('sinet.controllers').controller('officerRouteCtrl', Ctrl);
    Ctrl.$inject = ['$q', '$state', '$scope', 'uiGmapGoogleMapApi', 'mapDataResolve'];
    /* @ngInject */
    function Ctrl($q, $state, $scope, uiGmapGoogleMapApi, mapDataResolve) {
        var vm = $scope;
        //vm.markers = [];
        
        
        var latVal = mapDataResolve.latt,
            longVal = mapDataResolve.long;
        vm.markerIcon = "img/policeman.png";


        uiGmapGoogleMapApi.then(function (maps) {
            vm.map = {
                center: {
                    latitude: latVal,
                    longitude: longVal
                },
                zoom: 16,
                bounds: {}
            };
        });

        vm.options = {
            scrollwheel: true
        };

        var createRandomMarker = function (i, bounds, idKey) {

            if (idKey === null) {
                idKey = "id";
            }

            var latitude = latVal + (Math.random() * 0.0065600000000000);
            var longitude = longVal + (Math.random() * 0.0065833300000049300);
            var ret = {};
            if (i == 8) {
                ret = {
                    latitude: latitude,
                    longitude: longitude,
                    title: 'm' + i,
                    id: i,
                    icon: "img/policemanCentre.png"

                };
                return ret;

            } else {
                ret = {
                    latitude: latitude,
                    longitude: longitude,
                    title: 'm' + i,
                    id: i,
                    icon: "img/policeman.png"

                };
                //ret[idKey] = i;
                return ret;
            }
        };
        
        
        
        

        vm.randomMarkers = [];
        // Get the bounds from the map once it's loaded
        vm.$watch(function () {

            return vm.map.bounds;
        }, function (nv, ov) {

            // Only need to regenerate once
            if (!ov.southwest && !nv.southwest) {
                var markers = [];
                for (var i = 0; i < 15; i++) {
                    markers.push(createRandomMarker(i, vm.map.bounds));
                }
                vm.randomMarkers = markers;
            }
        }, true);


        $scope.officerRoute = vm;
    }
})();


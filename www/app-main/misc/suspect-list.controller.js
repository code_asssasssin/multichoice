(function () {
    'use strict';
    angular.module('sinet.controllers').controller('suspectListCtrl', Ctrl);
    Ctrl.$inject = ['$q', '$state', '$scope', 'authService', 'suspectService', 'localStorageService', 'localStorageKeyService', 'modalDialogService', 'appSettingSevice'];
    /* @ngInject */
    function Ctrl($q, $state, $scope, authService, suspectService, localStorageService, localStorageKeyService, modalDialogService, appSettingSevice) {
        var vm = this;
        var viewDir = appSettingSevice.viewDir;
        vm.suspectObj = {
            fname: "",
            lname: "",
            bDate: "",
            idType: "",
            idNumber: "",
            offenceType: "",
            loc: "",
            notes: "",
            imageData: "",
            thumbData: "",
            id: "",
            address: "",
            vehicleType: "",
            vehicleModel: "",
            vehicleMake: "",
            vImageData: "",
            chasisNo: "",
            engineNo: "",
            tagNumber: ""
        };

        vm.suspectDetails = function () {
            console.log('here');
            var storedSuspects = localStorageService.get(localStorageKeyService.suspectDataKey);
            return storedSuspects;
        };


        function gotoUrl(urlState) {
            $state.go(urlState);
        }

        $scope.suspectModalShow = function (obj) {

            setSuspectData(obj);
            modalDialogService.show(viewDir + "suspect-data-modal.html", $scope.suspectObj);
        };

        $scope.getImageIcon = function (imageData) {
            var img = imageData;
            return img;
        };



        function setSuspectData(obj) {
            $scope.suspectObj = obj;
        }

        $scope.suspectModalHide = function () {
            modalDialogService.hide();
        };

        angular.extend(this, {
            gotoUrl: gotoUrl,
            //setSuspectData: setSuspectData
        });

        $scope.suspects = this.suspectDetails();

        return vm;
    }
})();
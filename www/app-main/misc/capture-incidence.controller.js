(function () {
    'use strict';
    angular.module('sinet.controllers').controller('policeStationsController', Ctrl);
    Ctrl.$inject = ['$q', '$state', '$scope', 'localStorageService', 'localStorageKeyService', 'guidCreator', 'notificationService'];
    /* @ngInject */
    function Ctrl($q, $state, $scope, localStorageService, localStorageKeyService, guidCreator, notificationService) {
        var vm = $scope;

        vm.reportData = {
            lat: "",
            lng: "",
            incidenceType: "",
            description: "",
            image: "",
            video: "",
            address: "",
            id: ""
        };

        document.addEventListener("deviceready", function () {
            vm.captureIncidenceImage = function () {

                var options = {
                    quality: 80,
                    destinationType: Camera.DestinationType.DATA_URL,
                    //destinationType: Camera.DestinationType.FILE_URI,
                    sourceType: 1, // 0:Photo Library, 1=Camera, 2=Saved Photo Album
                    //sourceType: Camera.PictureSourceType.CAMERA,
                    allowEdit: false,
                    //encodingType: Camera.EncodingType.JPEG,
                    encodingType: 0, // 0=JPG 1=PNG
                    targetWidth: 80,
                    targetHeight: 80,
                    saveToPhotoAlbum: false,
                    correctOrientation: true
                };

                navigator.camera.getPicture(onSuccess, onFail, options);
            };

            function onSuccess(imgData) {
                var img = "data:image/jpeg;base64," + imgData;
                vm.reportData.image = img;
                vm.$apply();
            }

            function onFail(e) {
                //console.log("On fail " + e);
            }

        }, false);


        vm.createIncident = function () {

            if (navigator.geolocation) {

                navigator.geolocation.getCurrentPosition(function (pos) {
                    var geocoder = new google.maps.Geocoder();
                    var latlng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
                    geocoder.geocode({
                        'latLng': latlng
                    }, function (results, status) {
                        if (status === google.maps.GeocoderStatus.OK) {

                            var result = results[0];
                            vm.reportData.lat = pos.coords.latitude;
                            vm.reportData.lng = pos.coords.longitude;
                            vm.reportData.address = result.formatted_address;
                            vm.$apply();
                            var uuid = guidCreator.guid();
                            vm.reportData.id = uuid;
                            localStorageService.set(localStorageKeyService.incidentReportkey, vm.reportData);

                        }
                    });
                });
            }

            vm.reportData = {
                lat: "",
                lng: "",
                incidenceType: "",
                description: "",
                image: "",
                video: "",
                address: "",
                id: ""
            };
            notificationService.success("Thanks, Incident Has Been Reported");
        };

        return vm;
    }
})();
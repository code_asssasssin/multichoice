(function () {
    'use strict';
    angular.module('sinet', [
        'ionic',
        'toastr',
        'ngFileUpload',
        'ngCordova',
        'ui.map',
        'angularReverseGeocode',
        'uiGmapgoogle-maps',

        //'ui.bootstrap',
        //'ngAnimate',
        //'LocalStorageModule',
        //'angularGrid',

        ///////////////////////
        'sinet.run',
        'sinet.routes',
        'sinet.services',
        'sinet.directives',
        'sinet.controllers',
        'sinet.utilities'
        /////////////////////////
    ]);
    angular.module('sinet.run', []);
    angular.module('sinet.routes', []);
    angular.module('sinet.services', []);
    angular.module('sinet.directives', []);
    angular.module('sinet.controllers', []);
    angular.module('sinet.utilities', []);
})();
/*(function () {
    'use strict';
    angular.module('sinet.routes').config(Config);
    Config.$inject = ['uiGmapGoogleMapApiProvider'];
    function Config(uiGmapGoogleMapApiProvider) {
        uiGmapGoogleMapApiProvider.configure({
            //    key: 'your api key',
            v: '3.17',
            libraries: 'weather,geometry,visualization'
        });
    }
})();*/

(function() {
  'use strict';
  angular.module('sinet.run').run(Run);
  Run.$inject = ['$interval', 'mapDataService', 'authService', 'trackingDataService', 'localStorageService', '$rootScope'];
  /* @ngInject */
  function Run($interval, mapDataService, authService, trackingDataService, localStorageService, $rootScope) {
    authService.fillAuthData();
    $interval(function() {
      if (authService.checkAuth()) {
        mapDataService
          .getCurrPosition(false)
          .then(function(result) {
            var trackData = {};
            console.log(result);
            trackData.Latitude = result.Lat;
            trackData.Longitude = result.Long;
            trackData.UserId = authService.authentication.id;
            trackData.AddedDate = new Date();
            trackingDataService.addTracking(trackData).then(function(result) {
              console.log(result);
            });
          }, function() {

          });
      }

    }, 1000 * 60 * 30);

    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, $state) {
      var toStateData = toState.data || {};
      var requireLogin = toStateData.requireLogin || false;

      if (requireLogin) {
        if (!authService.checkAuth()) {
          event.preventDefault();
          $state.go('app.auth-login', {redirectTo: toState.name, params: angular.toJson(toParams)});
        }
      }
    });
  }
})();

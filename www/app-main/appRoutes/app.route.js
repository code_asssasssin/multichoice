(function() {
    'use strict';
    angular.module('sinet.routes').config(Config);
    Config.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

    function Config($stateProvider, $urlRouterProvider, $locationProvider) {

        var viewDir = 'app-main/views/';

        $urlRouterProvider.otherwise('/auth/login');
        $locationProvider.hashPrefix('!');
        //$locationProvider.html5Mode(true);
        $stateProvider
            .state('app', {
                url: "^/app",
                abstract: true,
                templateUrl: viewDir + "app.menu.html",
                controller: 'menuCtrl'
            })
            .state('app.home', {
                url: "^/home",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "app.home.html",
                        controller: "",
                    }
                },
                data: {
                    requireLogin: false,
                    roles: []
                }
            })
            .state('app.auth-login', {
                url: "^/auth/login",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "auth-login.html",
                        controller: 'LoginCtrl',
                        controllerAs: "auth",
                        data: {
                            requireLogin: false
                        }
                    }
                }
            })
            .state('app.register-user', {
                url: "^/create/user",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "register-user.html",
                        controller: 'registerUserCtrl',
                        controllerAs: "regUser",
                        title: "Register Users",
                        data: {
                            requireLogin: true,
                            role: ['Admin']

                        }
                    }
                },
                resolve: {
                    mapDataResolve: ['mapDataService', function(dt) {
                        return dt.getCurrPosition().then(function(result) {
                            return result;
                        }, function() {

                        });
                    }]
                }
            })
            .state('app.register-customer', {
                url: "^/create/customer",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "register-user.html",
                        controller: 'registerUserCtrl',
                        controllerAs: "regUser",
                        title: "Register Customer",
                        data: {
                            requireLogin: true,
                            role: ['SalesAgent']

                        }
                    }
                },
                resolve: {
                    mapDataResolve: ['mapDataService', function(dt) {
                        return dt.getCurrPosition().then(function(result) {
                            return result;
                        }, function() {

                        });
                    }]
                }
            })
            .state('app.add-sales', {
                url: "^/add/sales",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "customer-order.html",
                        controller: 'custOrderCtrl',
                        controllerAs: "custoOder",
                        data: {
                            requireLogin: true,
                            role: ['SalesAgent']

                        }
                    }
                },
                resolve: {
                    mapDataResolve: ['mapDataService', function(dt) {
                        return dt.getCurrPosition().then(function(result) {
                            return result;
                        }, function() {

                        });
                    }]
                }
            })
            .state('app.add-customer-sales', {
                url: "^/add/customer-sales",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "order.html",
                        controller: 'orderCtrl',
                        controllerAs: "order",
                        data: {
                            requireLogin: false,
                            role: ['SalesAgent']

                        }
                    }
                }
            })
            .state('app.assigned-jobs', {
                url: "^/jobs",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "assigned-jobs.html",
                        controller: 'assignedJobsCtrl',
                        title: "My Assigned Jobs",
                        data: {
                            requireLogin: true,
                            role: ['Installer']

                        }
                    }
                }
            })

        .state('app.direction', {
                url: "^/direction/:id",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "sales-direction.html",
                        controller: 'CustDirectionCtrl',
                        resolve: {
                            myCoordinate: ['mapDataService', function(dt) {
                                return dt.getCurrPosition().then(function(result) {
                                    return result;
                                }, function() {

                                });
                            }],
                            customerCoordinate: ['assignedJobsDataService', '$stateParams', function(jobs, params) {
                                console.log(params);
                                return jobs.getCustomerCoordinate(params.id).then(function(result) {
                                    return result;
                                }, function() {

                                });
                            }]
                        },
                        data: {
                            requireLogin: true,
                            role: ['Installer']

                        }
                    }
                }
            }).state('app.customer-list', {
                url: "^/customers",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "customers-list.html",
                        controller: 'custListCtrl',
                        title: "Add Sales",
                        data: {
                            requireLogin: true,
                            role: ['SalesAgent']

                        }
                    }
                }

            })
            .state('app.assign-sales-installation', {
                url: "^/assign-installation",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "open-installations.html",
                        controller: 'openInstallationCtrl',
                        title: 'Add/Change Installer',

                        data: {
                            requireLogin: true,
                            role: ['CustomerService']

                        }
                    }
                }

            }).state('app.change-installer', {
                url: "^/change/installer",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "assign-installer.html",
                        controller: 'changeInstallerCtrl',
                        data: {
                            requireLogin: true,
                            role: ['SalesAgent']

                        }
                    }
                }

            })
            .state('app.installers-location', {
                url: "^/installers/location",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "installers-location-map.html",
                        controller: 'installerLocationCtrl',
                        title: 'Nearest Agent',
                        data: {
                            requireLogin: true,
                            role: ['CustomerService']

                        }
                    }
                },
                resolve: {
                    mapDataResolve: ['mapDataService', function(dt) {
                        return dt.getCurrPosition().then(function(result) {
                            return result;
                        }, function() {

                        });
                    }]
                }

            })
            .state('app.auth-logout', {
                url: "^/logout",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "auth-login.html",
                        controller: 'logoffCtrl',
                        title: 'LogOut',
                        data: {
                            requireLogin: false,
                            role: ['SalesAgent', 'Admin', 'Installer']

                        }
                    }
                }

            });

    }
})();

(function () {
    'use strict';
    angular.module('sinet.routes').config(Config);
    Config.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider'];
    /* @ngInject */
    function Config($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
        //var viewDir = appSettingSevice.viewDir;
        var viewDir = "app-main/views/";

        $httpProvider.interceptors.push('Interceptors');

        $stateProvider

            .state('app.auth-login-modal', {
                url: '^/auth/modal-login',
                templateUrl: viewDir + 'auth-login-modal.html',
                controller: 'loginModalCtrl',
                controllerAs: 'loginModal',
                data: {
                    requireLogin: false
                }
            })
        
          
            .state('app.auth-forgot-pw', {
                url: '^/auth/forgot-pw',
                templateUrl: viewDir + 'auth-forgot-password.html',
                controller: 'loginCtrl',
                controllerAs: 'login',
                data: {
                    requireLogin: false
                }
            });
    }
})();
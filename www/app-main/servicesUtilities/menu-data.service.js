(function () {
    'use strict';
    angular.module('sinet.services').factory('menuService', menuData);
    menuData.$inject = ['$q', '$http'];
    /* @ngInject */
    function menuData($q, $http) {
        var service = this;
        service.menu = {};

        service.dstvmenu = {
            "title": "Main Menu",
            "detail": [
                {
                    "main_menu": {
                        "iconClass": "fa fa-user dark",
                        "menuText": "Admin",
                        "menuTextClass": "dark"
                    },
                    "sub_menu": [

                        {
                            "urlState": "app.register-user",
                            "iconLeftClass": "fa fa-angle-right dark",
                            "iconRightClass": "",
                            "menuText": "Register Users",
                            "menuTextClass": "dark"
                        }

                    ]
                },
                {
                    "main_menu": {
                        "iconClass": "fa fa-user dark",
                        "menuText": "Sales Agent",
                        "menuTextClass": "dark"
                    },
                    "sub_menu": [
                        {
                            "urlState": "app.register-user",
                            "iconLeftClass": "fa fa-angle-right dark",
                            "iconRightClass": "",
                            "menuText": "Register Customer",
                            "menuTextClass": "dark"
                },
                        {
                            "urlState": "app.customer-list",
                            "iconLeftClass": "fa fa-angle-right dark",
                            "iconRightClass": "",
                            "menuText": "Add Sales",
                            "menuTextClass": "dark"
                },
                        {
                            "urlState": "app.assign-sales-installation",
                            "iconLeftClass": "fa fa-angle-right dark",
                            "iconRightClass": "",
                            "menuText": "Assign Installation",
                            "menuTextClass": "dark"
                }

            ]
        },
                {
                    "main_menu": {
                        "iconClass": "fa fa-user dark",
                        "menuText": "Installer",
                        "menuTextClass": "dark"
                    },
                    "sub_menu": [
                        {
                            "urlState": "app.assigned-jobs",
                            "iconLeftClass": "fa fa-angle-right dark",
                            "iconRightClass": "",
                            "menuText": "My Assigned Jobs",
                            "menuTextClass": "dark"
                },
                        {
                            "urlState": "app.installers-location",
                            "iconLeftClass": "fa fa-angle-right dark",
                            "iconRightClass": "",
                            "menuText": "Nearest Agents",
                            "menuTextClass": "dark"
                }
                        
                        

            ]
        }
    ]
        };

        service.menu = function () {
            return service.dstvmenu;
        };

        //================
        return service;
    }
})();
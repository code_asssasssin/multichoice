(function () {
    'use strict';
    angular.module('sinet.services')
        .constant('appSettingSevice', {
            apiServiceBaseUri: "https://eval.espressologic.com/rest/pdyit/yojfe/v1/",
            viewDir: 'app-main/views/',
            clientId: 'app',
            authorizationDataKey: 'authorizationData'
        });
})();
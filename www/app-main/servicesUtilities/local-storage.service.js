(function () {
    'use strict';
    angular.module('sinet.services').factory('localStorageService', storeD);
    storeD.$inject = ['$q', '$window'];
    /* @ngInject */
    function storeD($q, $window) {

        var store = this;

        /*============================== PRE-CONFIGURE ==============================*/

        function isLocalStorageSupported() {
            try {
                return 'localStorage' in $window && $window['localStorage'] !== null;
            } catch (e) {
                console.log("Local Storage not supported");
                return false;
            }
        }

        function checkStorageKey(key) {
            if (!key) {
                console.log("Storage key is not defined");
                return false;
            }
            return true;
        }

        var loadFromStorage = function (storageKey) {
            if (isLocalStorageSupported() && checkStorageKey(storageKey)) {
                return angular.fromJson($window.localStorage.getItem(storageKey)) || null;
                //return $window.localStorage[key] || null;
            }
            return null;
        };

        var saveToStorage = function (storageKey, items) {
            if (isLocalStorageSupported() && checkStorageKey(storageKey)) {
                $window.localStorage.setItem(storageKey, angular.toJson(items));
                //$window.localStorage[key] = JSON.stringify(value);
                return true;
            }
            return false;
        };

        var deleteStorageItemsByKey = function (storageKey) {
            if (isLocalStorageSupported() && checkStorageKey(storageKey)) {
                $window.localStorage.removeItem(storageKey);
                return true;
            }
            return false;
        };

        var deleteAllStorage = function () {
            if (isLocalStorageSupported()) {
                $window.localStorage.clear();
                return true;
            }
            return false;
        };

        var saveSingle = function (storageKey, item) {
            var items = loadFromStorage(storageKey);
            items.push(item);
            return saveToStorage(storageKey, items);
        };

        /*========================== BASICS ===========================*/

        store.get = function (storageKey) {
            
            return loadFromStorage(storageKey);
        };

        store.set = function (storageKey, data) {
            var currentdata = this.get(storageKey);
            if (currentdata === null) {
                currentdata = [];
            }
            currentdata.push(data);
            return saveToStorage(storageKey, currentdata);
        };

        store.clearAll = function (storageKey) {
            return deleteAllStorage();
        };

        store.remove = function (storageKey) {
            return deleteStorageItemsByKey(storageKey);
        };

        /*========================= ADVANCED ===========================*/

        store.getById = function (storageKey, itemId) {
            var items = loadFromStorage(storageKey);
            var val = null;
            for (var i = 0; i < items.length; i++) {
                if (items[i]["id"] === itemId) {
                    val = items[i];
                    break;
                }
            }
            return val;
        };

        store.append = function (storageKey, inData, identifierProp) {

            if (!inData) {
                console.log("Invalid data");
                return;
            }

            // DECLEAR THE UNIQUE DATA IDENTIFIER
            var uniqueID = identifierProp || "id";

            //console.log('inData');
            //console.log(inData);

            var dbData = loadFromStorage(storageKey);

            //console.log('dbData Begin');
            //console.log(dbData);

            // CHECK IF STORAGE IS NOT NULL
            if (dbData !== null) {
                // CHECK IF dbData IS AN ARRAY
                if (!dbData.length) {
                    dbData = [];
                    dbData.push(dbData);
                }
            } else {
                // STORAGE IS NULL
                dbData = [];
            }

            // CHECK IS NOT AN ARRAY
            if (inData.length === undefined) {
                // DATA MUST HAVE AN [uniqueID] PROPERTY
                if (inData[uniqueID]) {
                    // CHECK AND REMOVE IF THE [uniqueID] IS THE SAME (DUPLICATE DATA)
                    for (var j = 0; dbData.length > j; j++) {
                        if (dbData[j][uniqueID] === inData[uniqueID]) {
                            dbData.splice(j, 1);
                            //break;
                        }
                    }
                    dbData.push(inData);
                    return save();
                } else {
                    console.log('Could not save to list, data must have a unique idenfier: \'' + uniqueID + '\' property...');
                    console.log('You can specify your custom unique identifier thus: localStorageService(storageKey, storageData, identifierID);');
                }
            } else {
                // DATA MUST HAVE AN [uniqueID] PROPERTY
                if (inData[0] && inData[0][uniqueID]) {

                    //console.log('before splice');
                    //console.log(dbData);
                    // LOOP THROUGH DATA, CHECK AND REMOVE IF THE ID IS THE SAME / ALREADY EXISTS (DUPLICATE DATA)
                    for (var i = 0; inData.length > i; i++) {

                        // console.log('current...');
                        //console.log(inData[i][uniqueID]);

                        var dbDataLen = dbData.length;

                        for (var m = 0; dbDataLen > m; m++) {
                            if (dbData[m][uniqueID] === inData[i][uniqueID]) {
                                console.log(dbData[m][uniqueID] + "===" + inData[i][uniqueID]);
                                dbData.splice(m, 1);
                                //dbData = dbData;
                                //break;
                            }
                        }
                    }
                    // LOOP THROUGH  INCOMING DATA AND ADD EACH OF THEM
                    for (var k = 0; inData.length > k; k++) {
                        // DATA MUST HAVE A [uniqueID] PROPERTY TO BE SAVED
                        if (inData[k][uniqueID]) {
                            dbData.push(inData[k]);
                        }
                    }
                    //console.log('after splice');
                    //console.log(dbData);
                    //SAVE TO LOCAL STORAGE 
                    return save();
                } else {
                    console.log('Could not save to list, invalid data or data don\'t have a unique idenfier: \'' + uniqueID + '\' property...');
                    console.log('You can specify your custom unique identifier thus: localStorageService(storageKey, storageData, identifierID);');
                }
            }

            function save() {
                //console.log('dbData End');
                //console.log(dbData);
                return saveToStorage(storageKey, dbData);
            }
            return false;
        };

        store.deleteById = function (storageKey, item) {
            var idProp = "id";
            var dataId = item[idProp] || item;
            if (dataId) {
                var items = loadFromStorage(storageKey);
                for (var i = 0; i < items.length; i++) {
                    if (items[i][idProp] === dataId) {
                        items.splice(i, 1);
                        break;
                    }
                }
                saveToStorage(storageKey, items);
                return $q.when(item);
            }
        };

        /***************************/
        store.storageAsset = {
            "authData": {
                "id": 1,
                "key": "auth_10BD066C-4F03-C70E-71C0-9743701510A3",
                "type": {}
            }
        };
        /*=========================================================*/
        return store;
    }
})();
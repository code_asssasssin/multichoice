(function () {
    'use strict';
    angular.module('sinet.utilities').service('Interceptors', Interceptor);
    Interceptor.$inject = ['$injector', '$q', 'localStorageService', 'appSettingSevice', '$timeout'];

    function Interceptor($injector, $q, storage, appSettingSevice, $timeout) {
        var state, auth, q, http;
        var authDataStoreKey = appSettingSevice.authorizationDataKey;
        var au = this;

        $timeout(function () {
            state = $injector.get('$state');
            http = $injector.get('$http');
            auth = $injector.get('authService');
        });

        au.request = function (config) {

            config.headers = config.headers || {};

            var authData = storage.get('authorizationData');

            if (authData) {

                if (authData[0].token) {
                    config.headers.Authorization = authData[0].token;

                }
            }

            return config;
        };

        au.responseError = function (rejection) {

            if (rejection.status !== 401) {
                return $q.reject(rejection);
            }

            var deferred = $q.defer();

            auth.loginModal()
                .then(function () {
                    deferred.resolve(http(rejection.config));
                })
                .catch(function () {
                    state.go('app.home');
                    deferred.reject(rejection);
                });

            return deferred.promise;
        };
    }
})();
(function () {
    'use strict';
    angular.module('sinet.services').factory('utilityService', utility);
    utility.$inject = ['$q'];
    /* @ngInject */
    function utility($q) {
        var util = this;
        util.getGui = function () {
            function generatePart() {
                var guidPartNumber = (Math.random() * 0x10000) | 0;
                return (guidPartNumber + 0x10000).toString(16).substring(1).toUpperCase();
            }
            return generatePart() + generatePart() + "-" + generatePart() + "-" + generatePart() + "-" + generatePart() + "-" + generatePart() + generatePart() + generatePart();
        };
        
        utility.getObjectById = function (a, obj) {
            for (var i = 0; i < a.length; i++) {

                if (a[i].Id == obj) {
                    return a[i];
                }
            }
            return 0;
        };

        return util;
    }
})();
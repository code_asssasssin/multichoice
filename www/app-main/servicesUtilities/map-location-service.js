(function () {
    'use strict';
    angular.module('sinet.controllers').factory('mapDataService', Ctrl);
    Ctrl.$inject = ['$q', 'localStorageKeyService', 'localStorageService'];
    /* @ngInject */
    function Ctrl($q, localStorageKeyService, localStorageService) {
        var service = this;
        var currPosition = {};
        var latlng = {
            lat: "",
            lng: ""
        };

        service.getCurrPosition = function (save) {
            var deferred = $q.defer();
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    currPosition.Lat = position.coords.latitude;
                    currPosition.Long = position.coords.longitude;
                    if (save === true) {
                        // SAVE TO STORAGE
                        saveLocation(currPosition);
                    }
                    deferred.resolve(currPosition);
                });
            } else {
                deferred.reject();
            }
            return deferred.promise;
        };

       


        function saveLocation(position) {
            var dateNow = new Date(),
                secs = (dateNow.getSeconds() + dateNow.getMilliseconds()),
                dataID = (Math.floor((Math.random() * 9776042557735532)) + secs);

            var dataStore = {
                "id": dataID,
                "latt": position.latt,
                "long": position.long,
                "officer": "officer-" + dataID,
                "dateTime": dateNow
            };

            var storeKey = localStorageKeyService.mapDataLogs;
            var store = localStorageService.append(storeKey, dataStore);
            var storeData = localStorageService.get(storeKey);

            // OPTION: CLEAR DATA ON LENGHT
            if (storeData.length) {
                if (storeData.length > 5) {
                    //var storeDel = localStorageService.remove(storeKey);
                    //console.log('localStorageService Data REMOVED!');
                }
                //console.log('localStorageService Data');
            }

            //console.log(storeData);
        }
        return service;
    }
})();
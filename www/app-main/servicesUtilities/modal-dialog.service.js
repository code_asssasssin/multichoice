(function () {
    'use strict';
    angular.module('sinet.services').service('modalDialogService', modal);
    modal.$inject = ['$ionicModal', '$rootScope'];
    /* @ngInject */
    function modal($ionicModal, $rootScope) {
        var modalControl = {};

        var init = function (url, parentScope) {
            var promise;
            var scoperVar = /*scoper ||*/ $rootScope.$new();
            scoperVar.suspectObj = parentScope;
            scoperVar.randomMarkers = parentScope;

            promise = $ionicModal.fromTemplateUrl(url, {
                scope: {},
                animation: 'slide-in-up'
            }).then(function (modal) {
                scoperVar.modal = modal;
                modal.show();
                //return modal;
            });

            modalControl.close = function () {
                return scoperVar.modal.hide();
            };

            scoperVar.$on('$destroy', function () {
                scoperVar.modal.remove();
            });

            return promise;
        };

        function hide() {
            modalControl.close();
        }

        return {
            init: init,
            show: init,
            open: init,
            hide: hide
        };
    }
})();
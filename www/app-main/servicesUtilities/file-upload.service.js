(function () {
    'use strict';
    angular.module('sinet.services').factory('fileUploadService', fileUpl);
    fileUpl.$inject = ['$q', '$upload', 'appAssetSevice'];
    /* @ngInject */
    function fileUpl($q, $upload, appAssetSevice) {
        var fFile = this;
        fFile.upload = [];

        fFile.uploadFiles = function (uploadUrl, filesData, fileId) {

            if (!options.uploadUrl || !options.filesData) return;

            var deferred = $q.defer();

            //uploadUrl, filesData and fileId in  => options

            //files: an array of files selected, each file has name, size, and type.
            for (var i = 0; i < options.filesData.length; i++) {
                var _file = options.filesData[i];
                (function (index) {
                    fFile.upload[index] = $upload.upload({
                        method: 'post',
                        url: options.uploadUrl,
                        file: _file,
                        params: {
                            fileId: options.fileId
                        },
                        headers: {
                            'Content-Type': undefined
                        }
                    }).progress(function (evt) {
                        console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
                    }).success(function (data, status, headers, config) {
                        deferred.resolve(data);
                    }).error(function (data, status, headers, config) {
                        deferred.reject(data);
                    });
                })(i);
            }
            return deferred.promise;
        };
        return fFile;
    }
})();
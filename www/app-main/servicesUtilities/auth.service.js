(function () {
    'use strict';
    angular.module('sinet.services').service('authService', authService);
    authService.$inject = ['$http', '$q', '$state', 'localStorageService', 'modalDialogService', 'appSettingSevice', '$location', '$timeout'];

    function authService($http, $q, $state, localStorageService, modalDialogService, appSettingSevice, $location, $timeout) {
        var service = this;
        var apiUrlBase = appSettingSevice.apiServiceBaseUri;
        var authDataStoreKey = appSettingSevice.authorizationDataKey;

        //////////////////////////////////
        // APIs
        //-----------------------------------

        service.loginDialog = null;

        service.authentication = {
            isAuth: false,
            email: "",
            expires: 0,
            id: "",
            name: "",
            roles: [],
            access_token: ""
        };

        service.$time = Date.now || function () {
            return +new Date();
        };

        service.createUser = function (userData) {
            var deferred = $q.defer();

            var data = {
                lastname: userData.lastname,
                firstname: userData.firstname,
                email: userData.email,
                username: userData.username,
                password: userData.password,
                contactPhone: userData.phone,
                roleID: userData.roleId
            };
            $http({
                method: 'post',
                url: apiUrlBase + 'CreateUser/',
                data: data
            }).then(function (result) {
                //console.log(result.data);
                deferred.resolve(result.data);
            }, function (result) {
                deferred.reject(result.data);
            });
            return deferred.promise;
        };

        service.loginModal = function () {
            //url, Ctrl, size
            return modalDialogService.show(appSettingSevice.viewDir + 'auth-login-modal.html');
        };

        service.login = function (loginData) {
            this.logOut();
            var data = JSON.stringify(loginData);

            var deferred = $q.defer();
            var self = this;
            $http.post(apiUrlBase + '@authentication', data).then(function (result) {

                var response = result.data;
                localStorageService.set('authorizationData', {
                    token: 'Espresso ' + response.apikey + ':1',
                    name: response.Name,
                    expires: new Date(response.expiration),
                    email: response.Email,
                    id: response.Id,
                    roles: response.Roles,
                });

                var data = localStorageService.get('authorizationData');

                self.authentication.isAuth = true;
                self.authentication.expiration = new Date(response.expiration);
                self.authentication.name = response.Name;
                self.authentication.email = response.Email;
                self.authentication.id = response.Id;
                self.authentication.roles = response.Roles;


                deferred.resolve(response);

            }, function (err, status) {
                self.logOut();
                deferred.reject(err);
            });

            return deferred.promise;

        };

        service.checkRole = function (dbRoles) {

            var userRoles = this.getRoles().toLowerCase();

            console.log(dbRoles);

            var dbRoleArray = userRoles.split(",");

            var roleIntersect = _.intersection(dbRoles, dbRoleArray);
            var result = _.size(roleIntersect) > 0;

            return result;

        };

        service.getRoles = function () {
            var authData = localStorageService.get('authorizationData');
            return authData[0].roles;
        };

        service.logOut = function () {
            localStorageService.remove('authorizationData');

            
            service.authentication.isAuth = false;
            service.authentication.email = "";
            service.authentication.expiration = 0;
            service.authentication.name = "";
            service.authentication.id = "";
            service.authentication.roles = [];
        };

        service.fillAuthData = function () {

            var authData = localStorageService.get('authorizationData');
            if (authData) {
                // && authData['.expires'] > new Date()
                service.authentication.isAuth = true;
                service.authentication.expiration = authData[0].expiration;
                service.authentication.email = authData[0].email;
                service.authentication.name = authData[0].name;
                service.authentication.id = authData[0].id;
                service.authentication.roles = authData[0].roles;
            }
        };

        service.setAuthData = function (auth) {
            service.authentication = auth;
            localStorageService.set(authDataStoreKey, auth);
        };

        service.checkAuth = function () {
            var authData = localStorageService.get('authorizationData');
            var curDate = new Date();


            if (authData) {

                return true;


            } else {
                return false;
            }

            return false;
        };

        service.checkUsername = function (username) {
            var deferred = $q.defer();
            $http.get(appSettingSevice.apiServiceBaseUri + 'account/checkifusernameexist?value=' + username)
                .then(function (result) {
                    deferred.resolve(result);
                });
            return deferred.promise;
        };

        service.UserList = function (role) {
            return $http.get(apiUrlBase + 'main:UserList?arg.role=' + role);
        };

        service.RolesList = function () {
            return $http.get(apiUrlBase + 'main:Roles');
        };

        service.getLoggedInUserId = function () {
            var userData = localStorageService.get(authDataStoreKey);

        };


    }
})();
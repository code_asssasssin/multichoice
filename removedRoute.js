(function () {
    'use strict';
    angular.module('sinet.routes').config(Config);
    Config.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

    function Config($stateProvider, $urlRouterProvider, $locationProvider) {

        var viewDir = 'app-main/views/';

        $urlRouterProvider.otherwise('/auth/login');
        $locationProvider.hashPrefix('!');
        //$locationProvider.html5Mode(true);
        $stateProvider
            .state('app', {
                url: "^/app",
                abstract: true,
                templateUrl: viewDir + "app.menu.html",
                controller: 'menuCtrl',
            })
            .state('app.home', {
                url: "^/home",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "app.home.html",
                        controller: "menuCtrl",
                        controllerAs: "menu"
                    }
                },
                data: {
                    requireLogin: false,
                    roles: []
                }
            })
            .state('app.auth-login', {
                url: "^/auth/login",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "auth-login.html",
                        controller: 'LoginCtrl',
                        controllerAs: "auth",
                        data: {
                            requireLogin: false
                        }
                    }
                }
            })
            .state('app.register-user', {
                url: "^/create/user",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "register-user.html",
                        controller: 'registerUserCtrl',
                        controllerAs: "regUser",
                        data: {
                            requireLogin: false,
                            role: ['admin', 'salesagent']

                        }
                    }
                }
            })
            .state('app.add-sales', {
                url: "^/add/sales",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "customer-order.html",
                        controller: 'custOrderCtrl',
                        controllerAs: "custOrder",
                        data: {
                            requireLogin: false,
                            role: ['admin', 'salesagent']

                        }
                    }
                }
            })
            .state('app.add-customer-sales', {
                url: "^/add/sales",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "order.html",
                        controller: 'orderCtrl',
                        controllerAs: "order",
                        data: {
                            requireLogin: false,
                            role: ['admin', 'salesagent']

                        }
                    }
                }
            })
            .state('app.assigned-jobs', {
                url: "^/jobs",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "assigned-jobs.html",
                        controller: 'assignedJobsCtrl',
                        data: {
                            requireLogin: false,
                            role: ['installer', 'salesagent']

                        }
                    }
                }
            }).state('app.direction', {
                url: "^/direction",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "sales-direction.html",
                        controller: 'CustDirectionCtrl',
                        data: {
                            requireLogin: false,
                            role: ['installer', 'salesagent']

                        }
                    }
                }
            }).state('app.customer-list', {
                url: "^/customers",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "customers-list.html",
                        controller: 'custListCtrl',
                        data: {
                            requireLogin: false,
                            role: ['salesagent', 'admin']

                        }
                    }
                }

            })
        .state('app.assign-sales-installation', {
                url: "^/assign-installation",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "open-installations.html",
                        controller: 'openInstallationCtrl',
                        data: {
                            requireLogin: false,
                            role: ['salesagent', 'admin']

                        }
                    }
                }

            })
         .state('app.change-installer', {
                url: "^/change/installer",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "assign-installer.html",
                        controller: 'changeInstallerCtrl',
                        data: {
                            requireLogin: true,
                            role: ['salesagent', 'admin']

                        }
                    }
                }

            })
        
        
        
       
            .state('app.installers-location', {
                url: "^/installers/location",
                views: {
                    'menuContent': {
                        templateUrl: viewDir + "installers-location-map.html",
                        controller: 'installerLocationCtrl',
                        data: {
                            requireLogin: false,
                            role: ['salesagent', 'admin', 'installer']

                        }
                    }
                },
                resolve: {
                    mapDataResolve: ['mapDataService', function (dt) {
                            return dt.getCurrPosition().then(function (result) {
                                return result;
                            }, function () {

                            });
                    }
                ]
                }
             })